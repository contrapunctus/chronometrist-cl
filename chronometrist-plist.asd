(defsystem     "chronometrist-plist"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Friendly and extensible personal time tracker - plist backend"
  :depends-on  (:chronometrist)
  :serial      t
  :components  ((:module "src/backend/"
                 :serial t
                 :components ((:file "sexp")
                              (:file "plist-group")))))
