(in-package :cl)

(defpackage :chronometrist.tests
  (:use :cl)
  (:local-nicknames (:cm :chronometrist)
                    (:ct :chronometrist.task)
                    (:lt :local-time)
                    (:r :rutils))
  (:export #:run))
