(defsystem     "chronometrist-clobber"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Friendly and extensible personal time tracker - Clobber backend"
  :depends-on  (:chronometrist :clobber)
  :serial      t
  :components  ((:module "src/backend/"
                 :components ((:file "clobber")))))
