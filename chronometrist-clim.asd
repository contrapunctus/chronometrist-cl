(defsystem     "chronometrist-clim"
  :version     "0.0.1"
  :serial      t
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Friendly and extensible personal time tracker - CLIM GUI"
  :depends-on  (:chronometrist :mcclim :anathema :alexandria
                :stealth-mixin :trivial-package-local-nicknames)
  :serial      t
  :components  ((:module "src/clim/"
                 :serial t
                 :components ((:file "package")
                              (:file "core")
                              (:file "table")
                              ;; (:file "pane-task-duration")
                              ;; (:file "pane-log")
                              ;; (:file "pane-gantt")
                              (:module "unified-pane"
                               :serial t
                               :components ((:file "package")
                                            (:file "common")
                                            (:file "fold-button")
                                            (:file "task")
                                            (:file "duration-today")
                                            (:file "history")
                                            (:file "timeline")
                                            (:file "display-pane")))
                              (:file "frame")
                              (:module "commands"
                               :serial t
                               :components ((:file "etc")
                                            (:file "cycling")
                                            (:file "interval")
                                            (:file "scrolling")
                                            (:file "task")))))))
