(in-package :chronometrist)

(defun setup-file-watch (&optional (callback #'refresh-file))
  "Arrange for CALLBACK to be called when the backend file changes."
  (let* ((backend    (active-backend))
         (file       (chronometrist:backend-file backend))
         (file-watch (backend-file-watch backend)))
    (unless file-watch
      (setq file-watch
            (file-notify-add-watch file '(change) callback)))))

(defmethod edit-backend ((backend file-backend-mixin))
  (find-file-other-window (chronometrist:backend-file backend))
  (goto-char (point-max)))

(defmethod reset-backend ((backend file-backend-mixin))
  (with-slots (hash-table file-watch
               rest-start rest-end rest-hash
               file-length last-hash) backend
    (reset-task-list backend)
    (when file-watch
      (file-notify-rm-watch file-watch))
    (setf hash-table  (to-hash-table backend)
          file-watch  nil
          rest-start  nil
          rest-end    nil
          rest-hash   nil
          file-length nil
          last-hash   nil)
    (setup-file-watch)))

(defmethod backend-empty-p ((backend file-backend-mixin))
  (let ((file (chronometrist:backend-file backend)))
    (or (not (proble-file file))
        (file-empty-p file))))

(defmethod memory-layer-empty-p ((backend file-backend-mixin))
  (with-slots (hash-table) backend
    (zerop (hash-table-count hash-table))))

(defmethod backend-modified-p ((backend file-backend-mixin))
  (with-slots (file) backend
    (buffer-modified-p
     (get-buffer-create
      (find-file-noselect file)))))

(define-backend elisp-sexp-backend (backend file-backend-mixin)
  ((rest-start
    :initarg :rest-start
    :initform nil
    :accessor backend-rest-start
    :documentation "Integer denoting start of first s-expression in file.")
   (rest-end
    :initarg :rest-end
    :initform nil
    :accessor backend-rest-end
    :documentation "Integer denoting end of second-last s-expression in file.")
   (rest-hash
    :initarg :rest-hash
    :initform nil
    :accessor backend-rest-hash
    :documentation "Hash of content between rest-start and rest-end.")
   (file-length
    :initarg :file-length
    :initform nil
    :accessor backend-file-length
    :documentation "Integer denoting length of file, as returned by `(point-max)'.")
   (last-hash
    :initarg :last-hash
    :initform nil
    :accessor backend-last-hash
    :documentation "Hash of content between rest-end and file-length."))
  (:documentation
   "Base class for any text file backend which stores s-expressions readable by Emacs Lisp.")
  (:default-initargs :extension "sexp"))

(defmethod initialize ((backend elisp-sexp-backend))
  (let ((file (backend-file backend)))
    (unless (probe-file file)
      (with-current-buffer (find-file-noselect file)
        (erase-buffer)
        (goto-char (point-min))
        (insert ";;; -*- mode: sexp; -*-\n\n")
        (write-file file)))
    backend))

(defmacro sexp-in-file (file &rest body)
  "Run BODY in a buffer visiting FILE, restoring point afterwards."
  (declare (indent defun) (debug t))
  `(with-current-buffer (find-file-noselect ,file)
     (save-excursion ,@body)))

(defun sexp-pre-read-check (buffer)
  "Return non-nil if there is an s-expression before point in BUFFER.
Move point to the start of this s-expression."
  (with-current-buffer buffer
    (and (not (bobp))
         (backward-list)
         (or (not (bobp))
             (not (looking-at-p "^[[:blank:]]*;"))))))

(defmacro loop-sexp-file (_for sexp _in file &rest loop-clauses)
  "`loop' LOOP-CLAUSES over s-expressions in FILE.
SEXP is bound to each s-expressions in reverse order (last
expression first)."
  (declare (indent defun) (debug 'loop))
  `(sexp-in-file ,file
     (goto-char (point-max))
     (loop with ,sexp
       while (and (sexp-pre-read-check (current-buffer))
                  (setq ,sexp (ignore-errors (read (current-buffer))))
                  (backward-list))
       ,@loop-clauses)))

(defmethod backend-empty-p ((backend elisp-sexp-backend))
  (sexp-in-file (chronometrist:backend-file backend)
    (goto-char (point-min))
    (not (ignore-errors
           (read (current-buffer))))))

(defun rest-start (file)
  (sexp-in-file file
    (goto-char (point-min))
    (forward-list)
    (backward-list)
    (point)))

(defun rest-end (file)
  (sexp-in-file file
    (goto-char (point-max))
    (backward-list 2)
    (forward-list)
    (point)))

(defun file-hash
    (start end &optional (file (chronometrist:backend-file (active-backend))))
  "Calculate hash of `file' between START and END."
  (sexp-in-file file
    (secure-hash 'sha1
                 (buffer-substring-no-properties start end))))

(defun file-change-type (backend)
  "Determine the type of change made to BACKEND's file.
    Return
    :append  if a new s-expression was added to the end,
    :modify  if the last s-expression was modified,
    :remove  if the last s-expression was removed,
        nil  if the contents didn't change, and
          t  for any other change."
  (with-slots
      (file file-watch
            ;; The slots contain the old state of the file.
            hash-table
            rest-start rest-end rest-hash
            file-length last-hash) backend
    (let* ((new-length    (file-length file))
           (new-rest-hash (when (and (>= new-length rest-start)
                                     (>= new-length rest-end))
                            (file-hash rest-start rest-end file)))
           (new-last-hash (when (and (>= new-length rest-end)
                                     (>= new-length file-length))
                            (file-hash rest-end file-length file))))
      ;; (debug-message "File indices - old rest-start: %s rest-end: %s file-length: %s new-length: %s"
      ;;          rest-start rest-end file-length new-length)
      (cond ((and (= file-length new-length)
                  (equal rest-hash new-rest-hash)
                  (equal last-hash new-last-hash))
             nil)
            ((or (< new-length rest-end) ;; File has shrunk so much that we cannot compare rest-hash.
                 (not (equal rest-hash new-rest-hash)))
             t)
            ;; From here on, it is implicit that the change has happened at the end of the file.
            ((and (< file-length new-length) ;; File has grown.
                  (equal last-hash new-last-hash))
             :append)
            ((and (< new-length file-length) ;; File has shrunk.
                  (not (sexp-in-file file
                         (goto-char rest-end)
                         (ignore-errors
                           (read (current-buffer)))))) ;; There is no sexp after rest-end.
             :remove)
            (t :modify)))))

(defun reset-task-list (backend)
  "Regenerate BACKEND's task list from its data.
Only takes effect if `task-list' is nil (i.e. the
user has not defined their own task list)."
  (unless task-list
    (setf (backend-task-list backend) (list-tasks backend))))

(defun add-to-task-list (task backend)
  "Add TASK to BACKEND's task list, if it is not already present.
Only takes effect if `task-list' is nil (i.e. the
user has not defined their own task list)."
  (with-slots (task-list) backend
    (unless (and (not task-list)
                 (member task task-list :test #'equal))
      (setf task-list
            (sort (cons task task-list)
                  #'string-lessp)))))

(defun remove-from-task-list (task backend)
  "Remove TASK from BACKEND's task list if necessary.
TASK is removed if it does not occur in BACKEND's hash table, or
if it only occurs in the newest plist of the same.

Only takes effect if `task-list' is nil (i.e. the
user has not defined their own task list).

Return new value of BACKEND's task list, or nil if
unchanged."
  (with-slots (hash-table task-list) backend
    (unless task-list
      (let (;; number of plists in hash table
            (ht-plist-count (loop with count = 0
                              for intervals being the hash-values of hash-table
                              do (loop for _interval in intervals
                                   do (incf count))
                              finally (return count)))
            ;; index of first occurrence of TASK in hash table, or nil if not found
            (ht-task-first-result (loop with count = 0
                                    for intervals being the hash-values of hash-table
                                    when (loop for interval in intervals
                                           do (incf count)
                                           when (equal task (getf interval :name))
                                           return t)
                                    return count)))
        (when (or (not ht-task-first-result)
                  (= ht-task-first-result ht-plist-count))
          ;; The only interval for TASK is the last expression
          (setf task-list (remove task task-list)))))))

(defmethod on-change ((backend elisp-sexp-backend) &rest fs-event)
  "Function called when BACKEND file is changed.
This may happen within Chronometrist (through the backend
protocol) or outside it (e.g. a user editing the backend file).

FS-EVENT is the event passed by the `filenotify' library (see `file-notify-add-watch')."
  (with-slots (file hash-table file-watch
               rest-start rest-end rest-hash
               file-length last-hash) backend
    (let-match* (((list _ action _ _) fs-event)
                 (file-state-bound-p (and rest-start rest-end rest-hash
                                          file-length last-hash))
                 (change      (when file-state-bound-p
                                (file-change-type backend)))
                 (reset-watch-p (or (eq action 'deleted)
                                    (eq action 'renamed))))
      (debug-message "[Method] on-change: file change type %s" change)
      ;; If only the last plist was changed, update hash table and
      ;; task list, otherwise clear and repopulate hash table.
      (cond ((or reset-watch-p
                 (not file-state-bound-p) ;; why?
                 (eq change t))
             (reset-backend backend))
            (file-state-bound-p
             (case change
               ;; A new s-expression was added at the end of the file
               (:append (on-add backend))
               ;; The last s-expression in the file was changed
               (:modify (on-modify backend))
               ;; The last s-expression in the file was removed
               (:remove (on-remove backend))
               ;; `case' returns nil if the KEYFORM is nil
               )))
      (setf rest-start  (rest-start file)
            rest-end    (rest-end file)
            file-length (file-length file)
            last-hash   (file-hash rest-end file-length file)
            rest-hash   (file-hash rest-start rest-end file)))))
