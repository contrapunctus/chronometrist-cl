(in-package :cl)
(defpackage :chronometrist.plist-group
  (:use :cl :trivia)
  (:local-nicknames (:cm :chronometrist)
                    (:t  :local-time)
                    (:al :alexandria))
  (:export #:backend))
(in-package :chronometrist.plist-group)

;; TODO - define `intervals' method

(cm:define-backend backend (cm:elisp-sexp-backend) ()
  (:default-initargs :extension "plg")
  (:documentation "Store records as plists grouped by date."))

(defun backward-read-sexp (buffer)
  (backward-list)
  (save-excursion (read buffer)))

(defmethod cm:latest-date-records ((backend backend))
  (sexp-in-file (cm:backend-file backend)
    (goto-char (point-max))
    (ignore-errors
      (backward-read-sexp (current-buffer)))))

#+(or)
(defmethod insert ((backend backend) plist
                   &key (save t)
                   &allow-other-keys)
  ;; (check-type plist plist)
  (debug-message "[Method] insert: %S" plist)
  (if (not plist)
      (error "%s" "`insert' was called with an empty plist")
      (sexp-in-file
       (cm:backend-file backend)
       (let-match* (((list plist-1 plist-2)   (split-interval plist))
                    ;; Determine if we need to insert a new plist group
                    (latest-plist-group  (latest-date-records backend))
                    (backend-latest-date (first latest-plist-group))
                    (date-today          (date-iso))
                    (insert-new-group    (not (equal date-today backend-latest-date)))
                    (start-date          (iso-to-date (getf plist :start)))
                    (new-plist-group-1   (if latest-plist-group
                                             (append latest-plist-group
                                                     (list (or plist-1 plist)))
                                             (list start-date (or plist-1 plist))))
                    (new-plist-group-2   (when (or plist-2 insert-new-group)
                                           (list date-today (or plist-2 plist)))))
         (goto-char (point-max))
         (when (not latest-plist-group)
           ;; first record
           (while (forward-comment 1) nil))
         (if (and plist-1 plist-2)
             ;; inactive, day-crossing record
             (progn
               (when latest-plist-group
                 ;; not the first record
                 (sexp-pre-read-check (current-buffer))
                 (sexp-delete-list))
               (funcall sexp-pretty-print-function new-plist-group-1 (current-buffer))
               (dotimes (_ 2) (default-indent-new-line))
               (funcall sexp-pretty-print-function new-plist-group-2 (current-buffer)))
             ;; active, or non-day-crossing inactive record
             ;; insert into new group
             (if (or (not latest-plist-group) ;; first record
                     insert-new-group)
                 (progn
                   (default-indent-new-line)
                   (funcall sexp-pretty-print-function new-plist-group-2 (current-buffer)))
                 ;; insert into existing group
                 (progn
                   (sexp-pre-read-check (current-buffer))
                   (sexp-delete-list)
                   (funcall sexp-pretty-print-function
                            new-plist-group-1
                            (current-buffer)))))
         (when save (save-buffer))
         t))))

(defun last-two-split-p (file)
  "Return non-nil if the latest two plists in FILE are split.
FILE must be a file containing plist groups, as created by
`plist-backend'.

Return value is either a list in the form
(OLDER-PLIST NEWER-PLIST), or nil."
  (sexp-in-file file
    (let* ((newer-group (progn (goto-char (point-max))
                               (backward-list)
                               (read (current-buffer))))
           (older-group (and (= 2 (length newer-group))
                             (backward-list 2)
                             (read (current-buffer))))
           ;; in case there was just one plist-group in the file
           (older-group (unless (equal older-group newer-group)
                          older-group))
           (newer-plist (second newer-group))
           (older-plist (first (last older-group))))
      (when (and older-plist newer-plist
                 (intervals-split-p older-plist newer-plist))
        (list older-plist newer-plist)))))

;; TODO - doesn't conform to START and END yet
(defmethod cm:intervals ((backend backend) &key start end)
  (with-open-file (in (cm:backend-file backend) :if-does-not-exist :error)
    (loop :with inactive :with active
          :for sexp = (read in nil :eof)
          :until (eq sexp :eof)
          :do (loop :for plist :in (rest sexp)
                    :for stop = (getf plist :stop)
                    :for interval
                      = (let ((name  (list (getf plist :name)))
                              (start (t:parse-timestring (getf plist :start)))
                              (stop  (and stop (t:parse-timestring stop)))
                              (props (al:remove-from-plist plist :name :start :stop)))
                          (make-instance 'cm:interval
                                         :names name :start start
                                         :stop stop  :properties props))
                    :when stop :do (push interval inactive)
                      :else :do (push interval active))
          :finally (return (values inactive active)))))

(defmethod cm:to-hash-table ((backend backend))
  (let ((file (cm:backend-file backend)))
    ;; (format t "file: ~a" file)
    (with-open-file (in file)
      (loop
        :with ht = (make-hash-table-1)
        :for sexp = (read in nil :eof)
        :until (eq sexp :eof) :do
          (let* ((iso-date    (first sexp))
                 (julian-date (iso-to-julian-date iso-date))
                 (intervals
                   (loop :for plist :in (rest sexp)
                         :collect
                         (let ((names (list (getf plist :name)))
                               (start (t:parse-timestring (getf plist :start)))
                               (stop  (getf plist :stop))
                               (props (plist-key-values plist)))
                           (cm:make-interval names start stop
                                             (and stop (t:parse-timestring stop))
                                             props)))))
            (setf (gethash julian-date ht) intervals))
        finally (return ht)))))

(defmethod cm:migrate ((input cm:backend) (output backend) &key (if-exists :error))
  (let ((file (backend-file output)))
    (delete-file file)
    (cm:initialize output)
    (reset-backend output)
    (sexp-in-file
     file
     (goto-char (point-max))
     (loop :for date :being :the hash-keys :in hash-table :using (hash-value plists)
           :do (insert (plist-pp (apply #'list date plists)) "\n")
           :finally (save-buffer)))))

(defmethod cm:on-add ((backend backend))
  "Function run when a new plist-group is added at the end of a
`chronometrist.plist-group:backend' file."
  (let-match (((cons date plist) (latest-date-records backend)))
    (setf (gethash date (hash-table backend)) plist)
    (add-to-task-list (getf plist :name) backend)))

(defmethod cm:on-modify ((backend backend))
  "Function run when the newest plist-group in a
`chronometrist.plist-group:backend' file is modified."
  (let-match* ((hash-table  (hash-table backend))
               ((cons date plists) (latest-date-records backend))
               (old-date    (ht-last-date hash-table))
               (old-plists  (gethash old-date hash-table)))
    (puthash date plists hash-table)
    (loop for plist in old-plists
          do (remove-from-task-list (getf plist :name) backend))
    (loop for plist in plists
          do (add-to-task-list (getf plist :name) backend))))

(defmethod cm:on-remove ((backend backend))
  "Function run when the newest plist-group in a
`chronometrist.plist-group:backend' file is deleted."
  (let* ((hash-table  (hash-table backend))
         (old-date    (ht-last-date hash-table))
         (old-plists  (gethash old-date hash-table)))
    (loop for plist in old-plists
          do (remove-from-task-list (getf plist :name) backend))
    (puthash old-date nil hash-table)))

(defmethod cm:list-tasks ((backend backend))
  (loop :for plist :in (cm:intervals backend)
        :collect (getf plist :name) :into names
        :finally (return
                   (sort (remove-duplicates names :test #'equal)
                         #'string-lessp))))

(defmethod cm:latest-interval ((backend backend))
  (if (last-two-split-p (cm:backend-file file))
      (apply #'plist-unify (last-two-split-p (cm:backend-file (active-backend))))
      (first (last (latest-date-records backend)))))

(defmethod cm:task-records-for-date
    ((backend backend) task date-ts &key &allow-other-keys)
  (check-type task string)
  (check-type date-ts ts)
  (loop for plist in (gethash (date-iso date-ts)
                              (backend-hash-table backend))
        when (equal task (getf plist :name))
          collect plist))
