(in-package :cl)
(defpackage :chronometrist.clobber
  (:use :cl)
  (:local-nicknames (:cm :chronometrist)
                    (:db :clobber)
                    (:t :local-time)
                    (:r :rutils))
  (:import-from :chronometrist
                #:make-interval #:midnight #:parse-timestring
                #:iso-to-julian-date
                #:interval #:name #:start #:stop #:properties
                #:interval #:intervals-split-p #:split-interval #:interval-unify)
  (:export #:backend
           ;; customizable variables
           #:*sqlite-properties-function*))
(in-package :chronometrist.clobber)

;; Migrate data from SQLite to Clobber
#+(or)
(let ((plg (make-instance 'chronometrist.plist-group:backend
                          :file #P"~/.emacs.d/chronometrist"))
      (clob (make-instance 'chronometrist.clobber:backend)))
  (chronometrist:initialize clob)
  (chronometrist:migrate plg clob :if-exists :overwrite))

(cm:define-backend backend (cm:backend cm:file-backend-mixin)
  ;; These slots are not yet in use
  ((%transaction-log :initarg :transaction-log
                     :accessor transaction-log
                     :documentation "Instance of `clobber::transaction-log'.")
   (%intervals :initarg :intervals
               :accessor intervals
               :documentation "List of instances of `chronometrist:interval'."))
  (:documentation "Store records in the Clobber object store.")
  (:default-initargs :extension "clob"))

#+(or)
(defmethod cm:copy-object ((backend backend))
  (make-instance 'backend
                 :task-list (cm:task-list backend)
                 :extension (cm:extension backend)
                 :file (cm:file-name backend)
                 :file-watch (cm:backend-file-watch backend)))

(db:define-save-info t:timestamp
  (:day t:day-of)
  (:sec t:sec-of)
  (:nsec t:nsec-of))

(db:define-save-info cm:interval
  (:names cm:names)
  (:start cm:start)
  (:stop cm:stop)
  (:properties cm:properties))

(defparameter *transaction-log* nil)

(defun apply-transaction (transaction)
  (apply (first transaction) (rest transaction)))

(defun execute (function &rest arguments)
  (apply function arguments)
  (db:log-transaction (cons function arguments) *transaction-log*))

(defun make-empty-vector ()
  (make-array 1 :fill-pointer 0
                :adjustable t
                :initial-element nil))

(defmethod cm:initialize ((backend backend))
  (setf *intervals* (make-empty-vector)
        *transaction-log*
        (db:open-transaction-log (cm:backend-file backend) #'apply-transaction))
  backend)

(defmethod cm:cleanup ((backend backend))
  (db:close-transaction-log *transaction-log*))

(defparameter *intervals* (make-empty-vector)
  "Vector of `interval' instances, in chronological order.")

(defun insert (interval)
  (vector-push-extend interval *intervals*))

(defmethod cm:insert ((backend backend) (interval interval) &key &allow-other-keys)
  (execute 'insert interval))

(defun update (old new)
  (setf *intervals*
        (substitute new old *intervals*
                    :test #'interval-equal?)))

(defmethod cm:update ((backend backend) (old interval) (new interval))
  (execute 'update old new))

(defmethod cm:remove ((backend backend) (interval interval) &key &allow-other-keys)
  (setf *intervals*
        (remove interval *intervals*
                :test #'cm:interval-equal?)))

;; readers
(defmethod cm:intervals ((backend backend) &key start end)
  (let* ((range-start (or start (cm:start (elt *intervals* 0))))
         (range-end   (or end (t:now)))
         inactive active)
    (loop :for interval :across *intervals*
          :when (eq :inactive
                    (cm:interval-in-range? interval
                                           range-start
                                           range-end))
            :collect interval :into inactive
          :when (eq :active
                    (cm:interval-in-range? interval
                                           range-start
                                           range-end))
            :collect interval :into active
          :finally (return (values inactive active)))))

(defmethod cm:latest-interval ((backend backend))
  )

(defmethod cm:active-days ((backend backend) task &key start end))
