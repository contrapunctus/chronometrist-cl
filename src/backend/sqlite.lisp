(in-package :cl)
(defpackage :chronometrist.sqlite
  (:use :cl)
  (:import-from :alexandria #:hash-table-keys)
  (:import-from :uiop #:strcat)
  (:import-from :local-time
                #:timestamp-to-unix
                #:unix-to-timestamp
                #:astronomical-julian-date
                #:now)
  (:import-from :sqlite
                #:connect #:disconnect
                #:execute-non-query #:execute-single #:execute-to-list
                #:last-insert-rowid
                #:with-open-database)
  (:import-from :sxql
                #:yield
                #:create-table #:foreign-key #:unique-key
                #:insert-into #:select #:= #:set= #:from #:order-by #:where
                #:left-join #:limit
                #:update)
  (:import-from :alexandria #:flatten)
  (:import-from :trivia
                #:let-match #:let-match* #:plist)
  (:local-nicknames (:cm :chronometrist)
                    (:r :rutils)
                    (:lt :local-time)
                    (:db :sqlite))
  (:export #:backend
           ;; customizable variables
           #:*sqlite-properties-function*))
(in-package :chronometrist.sqlite)

(loop :for (fn . alias)
        :in '((sqlite:execute-non-query . execute-statement)
              (sqlite:execute-single . query-cell)
              (sqlite:execute-single . query-row))
      :do (setf (fdefinition alias) (symbol-function fn)))

(cm:define-backend backend (cm:backend cm:file-backend-mixin)
  ((%connection :initform nil
                :initarg :connection
                :accessor connection))
  (:default-initargs :extension "sqlite")
  (:documentation "Store records in SQLite database."))

;; Is this really necessary given the use of `with-open-database'?
(defmethod initialize-instance :after ((backend backend) &rest initargs)
  "Initialize connection for BACKEND based on its file."
  (declare (ignore initargs))
  (let ((file (cm:backend-file backend)))
    (unless (connection backend)
      (setf (connection backend) (connect file)))))

(setf cm:*active-backend* (make-instance 'backend))

(defun execute-sxql (exec-fn sxql database)
  "Execute SXQL statement on DATABASE using EXEC-FN.

EXEC-FN must be a function accepting DATABASE, an SQL query string,
and zero or more arguments to the query. `sqlite:execute-single' and
`sqlite:execute-to-list' are two examples of such a function.

SXQL must be an SXQL-statement object acceptable to `sxql:yield'.

DATABASE must be a database object acceptable to the EXEC-FN, such as
that returned by `sqlite:connect'."
  (multiple-value-bind (string values)
      (yield sxql)
    (apply exec-fn database string values)))

(defmacro select-intervals (backend &rest query-clauses)
  "Return instances of `chronometrist:interval' from BACKEND which match QUERY-CLAUSES.

QUERY-CLAUSES must be acceptable to an `sxql:select' call."
  `(with-open-database (db (cm:backend-file ,backend))
     (let ((results
             (execute-sxql #'execute-to-list
                           (select (:name :start_time :stop_time :properties)
                             (from :intervals)
                             (left-join :interval_names :using (:name_id))
                             (left-join :properties :using (:prop_id))
                             ,@query-clauses)
                           db)))
       (when results
         (loop for (name start stop properties) in results
               when (and name start)
                 collect
                 (cm:make-interval name
                                   (unix-to-timestamp start)
                                   (and stop (unix-to-timestamp stop))
                                   properties))))))

(defun interval-id (backend interval)
  "Return interval-id from (SQLite) BACKEND for INTERVAL, or NIL if not found."
  (with-open-database (db (cm:backend-file backend))
    (let* ((name  (name interval))
           (start (timestamp-to-unix (cm:start interval)))
           (stop  (stop interval))
           (stop  (and stop (timestamp-to-unix stop))))
      ;; Urgh. If a variable evaluates to NIL, the SxQL expression
      ;; `(where (:= :column var))' is NOT equivalent to `(where
      ;; (:is-null :column))' /o\ It's the same even with the
      ;; `cl-sqlite' string interface. And thus we have this ridiculous
      ;; duplication.
      (if stop
          (execute-sxql #'execute-single
                        (select :interval_id
                          (from :intervals)
                          (where (:and (:= :name_id
                                           (select :name_id
                                             (from :interval_names)
                                             (where (:= :name name))))
                                       (:= :start_time start)
                                       (:= :stop_time stop)))
                          (order-by (:desc :interval_id)))
                        db)
          (execute-sxql #'execute-single
                        (select :interval_id
                          (from :intervals)
                          (where (:and (:= :name_id
                                           (select :name_id
                                             (from :interval_names)
                                             (where (:= :name name))))
                                       (:= :start_time start)
                                       (:is-null :stop_time)))
                          (order-by (:desc :interval_id)))
                        db)))))

(defun insert-names (backend interval)
  "Insert names of INTERVAL into (SQLite) BACKEND if they do not exist,
and return their name-ids."
  (with-open-database (db (cm:backend-file backend))
    (loop :for name :in (cm:names interval) :do
      (execute-non-query
       db "INSERT OR IGNORE INTO interval_names (name) VALUES (?);" name)
      :collect
      (execute-sxql #'execute-single
                    (select :name_id
                      (from :interval_names)
                      (where (:= :name name)))
                    db))))

(defmethod cm:initialize ((backend backend))
  (with-open-database (db (cm:backend-file backend))
    (loop
      :for expr
        :in (list
             ;; Properties are user-defined key-values stored as JSON.
             (create-table :properties
                 ((prop_id    :type 'integer :primary-key t)
                  (properties :type 'text :unique t :not-null t)))
             ;; An event is a timestamp with a name and optional properties.
             (create-table :event_names
                 ((name_id :type 'integer :primary-key t)
                  (name    :type 'text :unique t :not-null t)))
             (create-table :events
                 ((event_id :type 'integer :primary-key t)
                  (name_id  :type 'integer :not-null t)
                  (time     :type 'integer :unique t :not-null t)
                  (prop_id  :type 'integer))
               (foreign-key '(name_id) :references '(event_names name_id))
               (foreign-key '(prop_id) :references '(properties prop_id)))
             ;; An interval is a time range with a name and optional properties.
             (create-table :interval_names
                 ((name_id :type 'integer :primary-key t)
                  (name    :type 'text :unique t :not-null t)))
             (create-table :intervals
                 ((interval_id :type 'integer :primary-key t)
                  (start_time  :type 'integer :not-null t)
                  (stop_time   :type 'integer)
                  (prop_id     :type 'integer))
               (foreign-key '(prop_id) :references '(properties prop_id)))
             (create-table :interval_name_map
                 ((interval_id :type 'integer :not-null t)
                  (name_id     :type 'integer :not-null t))
               (foreign-key '(interval_id) :references '(intervals interval_id))
               (foreign-key '(name_id) :references '(interval_names name_id)))
             ;; A date contains one or more events and intervals. It may
             ;; also contain properties.
             (create-table :dates
                 ((date_id :type 'integer :primary-key t)
                  (date    :type 'integer :unique t :not-null t)
                  (prop_id :type 'integer))
               (foreign-key '(prop_id) :references '(properties prop_id)))
             (create-table :date_events
                 ((date_id  :type 'integer :not-null t)
                  (event_id :type 'integer :not-null t))
               (foreign-key '(date_id) :references '(dates date_id))
               (foreign-key '(event_id) :references '(events event_id)))
             (create-table :date_interval_map
                 ((date_id     :type 'integer :not-null t)
                  (interval_id :type 'integer :not-null t))
               (foreign-key '(date_id)     :references '(dates date_id))
               (foreign-key '(interval_id) :references '(intervals interval_id))))
      :do (execute-non-query db (yield expr))
      :finally
         (execute-non-query db
                            "CREATE VIEW view_intervals AS
SELECT
  interval_id,
  name,
  datetime(start_time, 'unixepoch', 'localtime'),
  datetime(stop_time, 'unixepoch', 'localtime'),
  properties
FROM intervals
LEFT JOIN interval_names USING (name_id)
LEFT JOIN properties USING (prop_id)
ORDER BY interval_id DESC;")
         (return backend))))

(defmethod cm:intervals ((backend backend) &key start end)
  (with-open-database (db (cm:backend-file backend))
    (when (execute-sxql #'execute-single (select :* (from :intervals)) db)
      (let* ((start      (or start (cm:start (first (select-intervals backend (limit 1))))))
             (start-unix (timestamp-to-unix start))
             (end-unix   (timestamp-to-unix (or end (now))))
             (inactive
               (select-intervals backend
                                 (where
                                  (:or ;; intervals which start within the range
                                   (:and (:>= :start_time start-unix)
                                         (:<= :start_time end-unix)
                                         (:not-null :stop_time))
                                   ;; intervals which end within the range
                                   (:and (:>= :stop_time start-unix)
                                         (:<= :stop_time end-unix))))))
             (active
               (select-intervals backend
                                 (where (:and (:<= :start_time end-unix)
                                              (:is-null :stop_time))))))
        (values inactive active)))))

;; (setf (cm:events day)
;;       (loop for (name time properties)
;;               in (execute-sxql
;;                   #'execute-to-list
;;                   (select (:name :time :properties)
;;                     (from :events)
;;                     (left-join :event_names :using (:name_id))
;;                     (left-join :properties :using (:prop_id))
;;                     (where (:in :event_id
;;                                 (select (:event_id)
;;                                   (from :date_events)
;;                                   (where (:= :date_id date-id))))))
;;                   connection)
;;             collect (make-instance
;;                      'cm:event
;;                      :name name
;;                      :time time
;;                      :properties (when properties
;;                                    (read-from-string properties))))
;;       (cm:properties day)
;;       (when properties
;;         (read-from-string properties)))

(defmethod cm:cleanup ((backend backend))
  (db:disconnect (connection backend))
  (setf (connection backend) nil))

(defmethod cm:to-hash-table (backend)
  )

(defun insert-properties (backend interval)
  "Insert properties from INTERVAL to (SQLite) BACKEND and return the prop-id of the new property.

If the properties already exist, insert nothing, and return the
prop-id of the existing property."
  (with-open-database (db (cm:backend-file backend))
    (let* ((plist  (cm:properties interval))
           (encoded-properties
             (if (functionp *sqlite-properties-function*)
                 (funcall *sqlite-properties-function* plist)
                 (write-to-string plist :escape t :pretty nil :readably t))))
      ;; (format t "properties: ~s~%" encoded-properties)
      (when plist
        (execute-non-query db
                           "INSERT OR IGNORE INTO properties (properties) VALUES (?);"
                           encoded-properties)
        (execute-sxql #'execute-single
                      (select :prop_id
                        (from :properties)
                        (where (:= :properties encoded-properties)))
                      db)))))

(defun sqlite-properties-to-json (plist)
  "Return PLIST as a JSON string."
  (json-encode
   ;; `json-encode' throws an error when it thinks
   ;; it sees "alists" which have numbers as
   ;; "keys", so we convert any cons cells and any
   ;; lists starting with a number to vectors
   (-tree-map (lambda (elt)
                (cond ((pp-pair-p elt)
                       (vector (car elt) (cdr elt)))
                      ((consp elt)
                       (vconcat elt))
                      (t elt)))
              plist)))

(defvar *sqlite-properties-function* nil
  "Function used to control the encoding of user key-values.
The function must accept a single argument, the plist of key-values.

Any non-function value results in key-values being inserted as
s-expressions in a text column.")

(defmethod cm:insert
    ((backend backend) (interval cm:interval) &key &allow-other-keys)
  (with-open-database  (db (cm:backend-file backend))
    (let-match (((or (list interval-1 interval-2) nil)
                 (cm:split-interval interval)))
      (loop
        :for interval
          :in (if (and interval-1 interval-2)
                  (list interval-1 interval-2)
                  (list interval))
        :do (let* ((start-unix  (timestamp-to-unix (cm:start interval)))
                   (stop-unix   (and (cm:stop interval)
                                     (lt:timestamp-to-unix stop)))
                   ;; insert interval properties if they do not exist
                   (prop-id     (insert-properties backend interval))
                   (name-id     (insert-names backend interval)))
              ;; insert an interval...
              (execute-non-query db
                                 #.(format nil "~@{~A~^~%~}"
                                           "INSERT OR IGNORE INTO intervals"
                                           "  (start_time, stop_time, prop_id)"
                                           "VALUES (?, ?, ?);")
                                 start-unix
                                 stop-unix
                                 prop-id))))))

(defmethod cm:update
    ((backend chronometrist.sqlite:backend) (old cm:interval) (new cm:interval))
  (with-open-database (db (cm:backend-file backend))
    (let* ((start-unix-2 (timestamp-to-unix (cm:start new)))
           (stop-2       (stop new))
           (stop-unix-2  (and stop-2 (timestamp-to-unix stop-2)))
           (interval-id  (interval-id backend old)))
      (execute-sxql #'execute-non-query
                    (sxql:update :intervals
                      (set= :start_time start-unix-2
                            :stop_time stop-unix-2
                            :prop_id (insert-properties backend new))
                      (where (:= :interval_id interval-id))
                      (limit 1))
                    db)
      #+(or)
      (unless (equal (cm:names old) (cm:names new))
        )
      (last-insert-rowid db))))

(defmethod cm:remove
    ((backend backend) (interval cm:interval) &key &allow-other-keys)
  (with-open-database (db (cm:backend-file backend))
    (let ((id (interval-id backend interval)))
      (execute-sxql #'execute-non-query
                    (sxql:delete-from :intervals
                      (where (:= :interval_id id))
                      (limit 1))
                    db))))

(defmethod cm:list-tasks ((backend backend))
  ;; (format *debug-io* "list-tasks (sqlite)")
  (with-open-database (db (cm:backend-file backend))
    (flatten
     (execute-sxql #'execute-to-list
                   (select :name
                     (from :interval_names)
                     (order-by (:asc :name)))
                   db))))

(defmethod cm:latest-interval ((backend backend))
  (select-intervals backend
                    (order-by (:desc :interval_id))
                    (limit 1)))

#+(or)
(defmethod cm:task-records-for-date
    ((backend backend) task date &key &allow-other-keys)
  (let ((list (execute-sxql
               #'execute-to-list
               (select (:name :start_time :stop_time :properties)
                 (from :intervals)
                 (left-join :interval_names :using (:name_id))
                 (left-join :properties :using (:prop_id))
                 (where (:and
                         (:in :interval_id
                              (select (:interval_id)
                                (from :date_intervals)
                                (where (:= :date_id
                                           (select (:date_id)
                                             (from :dates)
                                             (where (:= :date date)))))))
                         (:= :name task))))
               (connection backend))))
    (loop for (name start stop prop-string) in list
          collect (make-interval name start stop prop-string))))

;; select interval_id, name, datetime(start_time, 'unixepoch', 'localtime'), datetime(stop_time, 'unixepoch', 'localtime'), properties
;; from intervals
;; left join interval_names using (name_id)
;; left join properties using (prop_id)
;; where start_time >= 1657218600 and stop_time <= 1657305000
;; order by interval_id;

;; (adjust-timestamp (today) (set :hour 0) (set :minute 0))
;; (adjust-timestamp (today) (set :hour 0) (set :minute 0) (offset :hour 24))

(defmethod cm:active-days ((backend backend) task &key start end))
