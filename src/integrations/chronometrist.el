;;; chronometrist.el --- Integration for the Chronometrist time tracker

;;; Commentary:
;; This extension uses Emacs hooks to automate time tracking for
;; Chronometrist.
;;
;; This is just a prototype at the moment, which logs events as
;; s-expressions to a file. When finished, it will call the
;; Chronometrist CLI instead, so as to manipulate the Chronometrist
;; database.

(defvar chronometrist-auto-file (locate-user-emacs-file "chronometrist-auto.el"))

(defvar chronometrist-task-directory-alist
  '(("Programming" "/media/data/contrapunctus/Documents/Text Files/programming/"))
  "Alist of tasks and regular expressions.

A task may be associated with any number of regular expressions.
These are matched against file paths to determine the task
associated with the file.")

(defun chronometrist-task-for-file (file)
  (cl-loop for (task . regexen) in chronometrist-task-directory-alist
    when (cl-loop for re in regexen
           when (string-match-p re file)
           return task)
    return it))

;;; File modification and saving

(defun chronometrist-on-first-change ()
  (when-let*
      ;; Proceed only if there's a file associated with this buffer.
      ((file (buffer-file-name (current-buffer)))
       ;; Not a necessary condition once we move to calling a CLI
       (task (chronometrist-task-for-file file))
       (list `(file-edit :file ,(file-name-nondirectory file)
                         :task ,task
                         :time ,(format-time-string "%FT%T%z"))))
    (chronometrist-sexp-in-file chronometrist-auto-file
      (goto-char (point-max))
      (insert (format "%S\n" list))
      (save-buffer))))

;; (add-hook 'first-change-hook #'chronometrist-on-first-change)

(defun chronometrist-on-save ()
  (when-let*
      ;; Proceed only if there's a file associated with this buffer.
      ((file (buffer-file-name (current-buffer)))
       ;; Not a necessary condition once we move to calling a CLI
       (task (chronometrist-task-for-file file))
       (list `(file-save :file ,(file-name-nondirectory file)
                         :task ,task
                         :time ,(format-time-string "%FT%T%z"))))
    (chronometrist-sexp-in-file chronometrist-auto-file
      (goto-char (point-max))
      (insert (format "%S\n" list))
      (save-buffer))))

;; (add-hook 'after-save-hook #'chronometrist-on-save)

(defvar chronometrist--last-buffer nil)

;; TODO - check if already clocked in

;; (defun chronometrist-on-file-view ()
;;   (unless (minibufferp (current-buffer))
;;     (when-let*
;;         ;; Proceed only if there's a file associated with this buffer.
;;         ((current-buffer  (current-buffer))
;;          (file            (buffer-file-name current-buffer))
;;          ;; Only clock in if there's a new buffer
;;          (buffer-switched (not (equal chronometrist--last-buffer current-buffer)))
;;          ;; Not a necessary condition once we move to calling a CLI
;;          (task            (chronometrist-task-for-file file))
;;          (list           `(file-view :file ,(file-name-nondirectory file)
;;                                      :task ,task
;;                                      :time ,(format-time-string "%FT%T%z"))))
;;       (chronometrist-sexp-in-file chronometrist-auto-file
;;         (goto-char (point-max))
;;         (insert (format "%S\n" list))
;;         (save-buffer)))
;;     (setq chronometrist--last-buffer (current-buffer))))

;; (add-hook 'buffer-list-update-hook #'chronometrist-on-file-view)

;; TODO - check if already clocked in
(defun chronometrist-on-file-view ()
  ;; (when (minibufferp (current-buffer))
  ;;   (message "minibuffer file %S" (buffer-file-name (current-buffer))))
  (let* ;; Proceed only if there's a file associated with this buffer.
      ((current-buffer  (current-buffer))
       (file            (buffer-file-name current-buffer))
       ;; Only clock in if there's a new buffer
       (buffer-switched (not (equal chronometrist--last-buffer current-buffer)))
       ;; Not a necessary condition once we move to calling a CLI
       (task            (and file (chronometrist-task-for-file file)))
       (list           `(file-view :file ,(and file (file-name-nondirectory file))
                                   :task ,task
                                   :time ,(format-time-string "%FT%T%z"))))
    (cond ((not file) nil)
          ((and buffer-switched task)
           (chronometrist-sexp-in-file chronometrist-auto-file
             (goto-char (point-max))
             (insert (format "%S\n" list))
             (save-buffer))
           (setq chronometrist--last-buffer (current-buffer))))))

;;; idle timer

(defvar chronometrist-idle-duration (* 5 60)
  "Seconds after which we clock out. If nil, don't clock out on idle.")

(defun chronometrist-clock-out-on-idle ()
  (chronometrist-sexp-in-file chronometrist-auto-file
    (goto-char (point-max))
    (insert (format "%S\n" `(idle :duration ,chronometrist-idle-duration
                                  :time ,(format-time-string "%FT%T%z"))))
    (save-buffer)))

(defvar chronometrist--timer nil)

(defun chronometrist-start-idle-timer ()
  (setq chronometrist--timer
        (run-with-idle-timer chronometrist-idle-duration t #'chronometrist-clock-out-on-idle)))

(defun chronometrist-stop-idle-timer ()
  (when chronometrist--timer
    (cancel-timer chronometrist--timer)
    (setq chronometrist--timer nil)))

(defun chronometrist-restart-idle-timer ()
  (when chronometrist--timer
    (chronometrist-stop-idle-timer)
    (chronometrist-start-idle-timer)))

(define-minor-mode chronometrist-minor-mode
  nil :global t
  (cond (chronometrist-minor-mode
         (chronometrist-restart-idle-timer)
         (add-hook 'window-configuration-change-hook #'chronometrist-on-file-view))
        (t (chronometrist-stop-idle-timer)
           (remove-hook 'window-configuration-change-hook #'chronometrist-on-file-view))))

(provide 'chronometrist)

;;; chronometrist.el ends here
