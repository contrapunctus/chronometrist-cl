(in-package :chronometrist-clim.log)

(defclass interval-table (ui:table
                          ui:time-range-mixin) ()
  (:default-initargs :schema
   (mapcar #'make-instance
           '(ui:index
             ui:task
             properties interval-duration start stop))))

(defclass pane (ui:pane)
  ((%tables :initarg :tables
            :accessor tables
            :initform (list (make-instance 'interval-table))))
  (:documentation
   "A pane displaying a log of intervals and events happening in a
   time range (default - today)."))

(defmethod ui:display-cell
    (table (column ui:task) (index integer)
     frame (pane pane) &key interval start end &allow-other-keys)
  (let ((name (name interval)))
    (format pane "~A" name)
    name))

(defclass properties (ui:cell) ()
  (:default-initargs :label "Properties"))

(defmethod ui:display-cell
    (table (column properties) (index integer)
     frame (pane pane) &key interval start end &allow-other-keys)
  (let ((properties (properties interval)))
    (format pane "~{~S~^ ~}" properties)
    properties))

(defclass interval-duration (ui:cell) ()
  (:default-initargs :label "Duration"))

(defmethod ui:display-cell
    (table (column interval-duration) (index integer) frame (pane pane)
     &key interval start end &allow-other-keys)
  (let ((duration
          (cond ((stop interval)
                 (interval-duration interval))
                ;; We don't change `(stop interval)', so that the `stop' column
                ;; can show a blank for active intervals.
                ((timestamp>= end (now))
                 (let ((o (copy-object interval)))
                   (setf (stop o) (now))
                   (interval-duration o)))
                (t (let ((o (copy-object interval)))
                     (setf (stop o) end)
                     (interval-duration o))))))
    (format-seconds pane (duration-format pane) display-cell-duration)
    duration))

(defclass start (ui:cell) ()
  (:default-initargs :label "From"))

(defmethod ui:display-cell
    (table (column start) (index integer) frame (pane pane)
     &key interval start end &allow-other-keys)
  (let ((start (start interval)))
    (format pane "~A"
            (format-timestring start (time-format pane)))
    start))

(defclass stop (ui:cell) ()
  (:default-initargs :label "To"))

(defmethod ui:display-cell
    (table (column stop) (index integer)
     frame (pane pane) &key interval start end &allow-other-keys)
  (let ((stop (stop interval)))
    (if (stop interval)
        (format pane "~A"
                (format-timestring display-cell-stop (time-format pane)))
        (format pane ""))
    stop))

#+(or)
(defmethod ui:table-rows ((table interval-table) frame pane)
  (loop for interval in intervals
        for index from 0
        when (zerop index)
          collect (mapcar #'label schema)
        else
          collect (loop for column in (schema table)
                        collect (display-cell table column index
                                           frame pane :interval interval))))

;; We need to present each interval object as a row, which means we
;; can't use `table-rows' for this one.
(defmethod ui:display-pane (frame (pane pane))
  (let ((datetime-format  (datetime-format pane)))
    ;; (format *debug-io* "start: ~A end: ~A~%" start end)
    (loop for table in (tables pane) do
      (bind ((start    (range-start table))
             (end      (range-end table))
             (inactive active (intervals (active-backend) :start start :end end)))
        (format pane
                (if (or inactive active)
                    "Intervals between ~A and ~A~%"
                    "No intervals to show between ~A and ~A.~%")
                (format-timestring start datetime-format)
                (format-timestring end datetime-format))
        (formatting-table (pane)
          (loop
            for interval in (append inactive active)
            for index from 0 do
              (formatting-row (pane)
                (with-output-as-presentation (pane interval 'interval)
                  (loop for column in (schema table) do
                    (if (zerop index)
                        ;; print header row
                        (formatting-cell (pane)
                          (format pane "~A" (label column)))
                        ;; print other rows
                        (formatting-cell (pane)
                          (let* ((kwargs (list :interval interval :start start :end end))
                                 (data   (apply #'ui:display-cell
                                                table column index frame pane kwargs)))
                            (apply #'ui:display-cell
                                   table column index data frame pane kwargs)))))))))))))
