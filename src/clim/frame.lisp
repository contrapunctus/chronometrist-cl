(in-package :chronometrist-clim)

(defmacro make-pane-helper (type &optional accessor)
  `(multiple-value-bind (pane stream)
       (make-clim-stream-pane :type (quote ,type) :scroll-bars t)
     (setf (,(or accessor type) *application-frame*) stream)
     pane))

(define-application-frame chronometrist ()
  ((%unified-pane :initarg :unified-pane
                  :accessor unified-pane)
   (%start-time :initarg :start-time
                :accessor start-time))
  (:menu-bar t)
  (:pointer-documentation t)
  (:panes (unified (make-pane-helper chronometrist-clim.unified:pane unified-pane))
          (interactor :interactor
                      :foreground (at:style-foreground-ink ats:*default*)
                      :background (at:style-background-ink ats:*default*)
                      :height (graft-height (find-graft))
                      :width (graft-width (find-graft))))
  (:layouts
   (default unified)
   (vertical-interactor
    (vertically ()
      (4/5 unified)
      (1/5 interactor)))
   (horizontal-interactor
    (horizontally ()
      (4/5 unified)
      (1/5 interactor)))))

(defun update-com-next-state (pane)
  (let ((range-end (range-end pane))
        (today-end (t:adjust-timestamp (c:midnight) (offset :hour 24))))
    (setf (command-enabled 'com-next *application-frame*)
          (not (t:timestamp>= range-end today-end)))))

(defmethod frame-exit :before (frame)
  (c:cleanup (c:active-backend)))

(defun run ()
  (let ((time  (get-universal-time))
        (init  (c:global-value c:*init-pathname*))
        (backend (c:active-backend)))
    (c:load-configuration)
    (if (probe-file init)
        (load init)
        (format *error-output* "Configuration file ~S does not exist." init))
    (ensure-directories-exist (c:global-value c:*database-directory*))
    (c:initialize backend)
    (find-application-frame 'chronometrist :start-time time)))

(defmethod note-frame-enabled :after (frame-manager (frame chronometrist))
  (update-interval-command-state)
  (update-com-next-state (unified-pane frame)))
