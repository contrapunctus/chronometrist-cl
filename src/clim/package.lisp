(in-package :cl)
(defpackage :chronometrist-clim
  (:use :clim :clim-lisp)
  (:shadow #:pane)
  (:local-nicknames (:c :chronometrist)
                    (:ct :chronometrist.task)
                    (:at :anathema)
                    (:ats :anathema.style)
                    (:t  :local-time)
                    (:r :rutils)
                    (:pat :trivia)
                    (:al :alexandria)
                    (:sm :stealth-mixin))
  (:export #:fold-mixin
           #:folded?)

  (:export #:task-mixin
           #:cursor
           #:view)

  (:export #:time-range-mixin
           #:range-start
           #:range-end)

  (:export #:pane
           #:time-format #:date-format #:datetime-format #:duration-format
           #:display-pane
           #:pane-range-today)

  ;; presentation types
  (:export #:interval #:intervals
           #:task-name
           #:duration #:durations)

  ;; tables
  (:export #:table #:schema #:data

           #:nest-mixin #:children #:depth
           #:label-mixin #:label
           #:index-mixin #:index

           #:row #:foldable-row #:cells

           #:column #:label #:index-column #:task-name-column

           #:cell #:header #:display-cell)
  ;; frame
  (:export #:chronometrist #:unified-pane #:run #:start-time)

  ;; commands
  (:export #:com-refresh))

(defpackage :chronometrist-clim.task-duration
  (:use :clim :clim-lisp)
  (:shadow #:pane)
  (:local-nicknames (:ui :chronometrist-clim))
  (:export #:pane))

(defpackage :chronometrist-clim.log
  (:use :clim :clim-lisp)
  (:shadow #:pane)
  (:local-nicknames (:ui :chronometrist-clim))
  (:export #:pane))

(defpackage :chronometrist-clim.gantt
  (:use :clim :clim-lisp)
  (:shadow #:pane)
  (:local-nicknames (:c  :chronometrist)
                    (:ui :chronometrist-clim)
                    (:at :anathema)
                    (:ats :anathema.style)
                    (:al :alexandria)
                    (:t  :local-time)
                    (:d :format-seconds)
                    (:r :rutils))
  (:export #:pane))
