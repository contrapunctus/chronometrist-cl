(in-package :chronometrist-clim)

(defclass fold-mixin ()
  ((%folded? :initarg :folded?
             :initform nil
             :accessor folded?
             :documentation "Non-nil if this object is folded.")))

(sm:define-stealth-mixin task-mixin (fold-mixin) ct:task
  ((%cursor :initarg :cursor :accessor cursor :initform nil)
   (%view :initarg :view :accessor view :initform nil)))

(defclass edit-view (view) ()
  (:documentation "View used when editing an object."))

(defclass time-range-mixin ()
  ((%range-start
    :initarg :range-start
    :accessor range-start
    :initform (c:midnight)
    :documentation
    "The starting time for the information displayed in this entity, as
   an instance of `local-time:timestamp'.")
   (%range-end
    :initarg :range-end
    :accessor range-end
    :initform (t:adjust-timestamp (c:midnight)
                (offset :hour 24))
    :documentation
    "The ending time for the information displayed in this entity, as
   an instance of `local-time:timestamp'.")))

(defclass pane (application-pane time-range-mixin)
  ((%date-format
    :initarg :date-format
    :accessor date-format
    :initform '(:short-weekday ", " (:day 2) #\space :short-month #\space (:year 4))
    :documentation
    "How to display time in this pane, as a list acceptable to
    `local-time:format-timestring'.")
   (%time-format
    :initarg :time-format
    :accessor time-format
    :initform '((:hour 2) ":" (:min 2) ":" (:sec 2))
    :documentation
    "How to display time in this pane, as a list acceptable to
    `local-time:format-timestring'.")
   (%datetime-format
    :initarg :datetime-format
    :accessor datetime-format
    :initform local-time:+rfc-1123-format+
    :documentation
    "How to display date and time in this pane, as a list acceptable
    to `local-time:format-timestring'.")
   (%duration-format
    :initarg :duration-format
    :accessor duration-format
    :initform "~2h:~2,'0m:~2,'0s"
    :documentation
    "The format string used for durations in this pane, acceptable to
    `format-seconds'."))
  (:default-initargs :display-function 'display-pane
                     :foreground (at:style-foreground-ink ats:*default*)
                     :background (at:style-background-ink ats:*default*)
                     :incremental-redisplay t)
  (:documentation "Abstract class for Chronometrist panes."))

(defgeneric display-pane (frame stream)
  (:documentation "Display a Chronometrist application pane.
PANE-NAME must be the `pane-name' as a keyword."))

(define-presentation-type interval (activep))

(define-presentation-method presentation-typep (object (type interval))
  (eq (not (stop object)) activep))

(define-presentation-type intervals () :inherit-from '(sequence interval))

(define-presentation-type task-name () :inherit-from 'string)

(define-presentation-method present
    (object (type task-name) stream view &key acceptably for-context-type)
  (format stream "~A" object))

(define-presentation-type duration () :inherit-from 'integer)
(define-presentation-type durations () :inherit-from '(sequence duration))

(defun pane-range-today (pane)
  (let* ((rs (range-start pane))
         (re (range-end pane))
         (today-start (c:midnight))
         (today-end   (t:adjust-timestamp today-start (offset :hour 24))))
    (cond ((and (t:timestamp> rs today-start)
                (t:timestamp> re today-end))
           :future)
          ((and (t:timestamp> today-start rs)
                (t:timestamp> today-end re))
           :past)
          ((and (t:timestamp= today-start rs)
                (t:timestamp= today-end re))
           :today))))
