(in-package :chronometrist-clim.unified)
(nr:in-readtable readtable)

(defun make-view (&optional color)
  (make-instance 'table-row-view :color color))

(defclass table (ui:table)
  ((%date :initarg :date
          :accessor date))
  (:default-initargs
   :schema
   (mapcar #'make-instance
           '(fold-button-column ui:index-column ui:task-name-column
             duration-today-column past-durations-column timeline-column))))

(defun find-column (type pane)
  (find-if λ(typep _ type) (ui:schema (table pane))))

(defclass interval-cache-mixin ()
  ((%interval-cache :initarg :interval-cache
                    :accessor interval-cache)))

(defmethod interval-cache ((object interval-cache-mixin))
  (if (slot-boundp object '%interval-cache)
      (slot-value object '%interval-cache)
      (setf (interval-cache object)
            ;; XXX - include active intervals
            (c:intervals (c:active-backend)
                         :start (ui:range-start object)
                         :end (ui:range-end object)))))

(defun reset-interval-caches ()
  ;; `reset-interval-caches' may be called during non-interactive use,
  ;; in which case there will be no application frame...
  (al:when-let*
      ((frame   (find-application-frame 'ui:chronometrist :create nil :activate nil))
       (pane    (ui:unified-pane frame))
       (history (find-column 'past-durations-column pane)))
    (slot-makunbound pane '%interval-cache)
    (slot-makunbound history '%interval-cache)
    ;; (redisplay-frame-pane frame pane)
    ;; (execute-frame-command frame '(ui:com-refresh))
    ))

(defmethod c:insert :after ((backend c:backend) object &key &allow-other-keys)
  (reset-interval-caches))

(defmethod c:update :after ((backend c:backend) old new)
  (reset-interval-caches))

(defmethod c:remove :after ((backend c:backend) object &key &allow-other-keys)
  (reset-interval-caches))

(defmethod c:migrate :after
    ((in c:backend) (out c:backend) &key if-exists interval-function &allow-other-keys)
  (reset-interval-caches))

(defclass pane (ui:pane interval-cache-mixin)
  ((%table :initarg :table
           :accessor table
           :initform (make-instance 'table))
   (%max-duration-today
    :initarg :max-duration-today
    :accessor max-duration-today
    :documentation
    "Highest total task duration on the current day, as integer seconds."))
  (:default-initargs :time-format '((:hour 2) ":" (:min 2)))
  (:documentation
   "A pane displaying a list of tasks and the time spent on them today."))

(defclass row (ui:foldable-row ui:nest-mixin)
  ((%task :initarg :task
          :accessor task
          :initform nil))
  (:default-initargs :folded? nil))

(defun indent (pane row)
  (al:when-let ((task (task row)))
    (format pane (make-string (* 3 (ct:depth (task row)))
                              :initial-element #\Space))))

(defmethod ui:display-cell
    (frame (pane pane) table (column ui:index-column) row view)
  (let ((cell (make-instance 'ui:index :index (ui:index row)
                                       :label (ui:label column))))
    (formatting-cell ()
      (indent pane row)
      (present cell 'ui:index))))
