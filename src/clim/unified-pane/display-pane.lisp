(in-package :chronometrist-clim.unified)
(nr:in-readtable chronometrist:readtable)

(defun display-tree (tree frame pane table)
  (let ((index 1))
    (labels
        ;; display a row for each task
        ((helper (tree)
           (let ((task (first tree)))
             (formatting-row ()
               ;; display a cell for each column
               (loop :for column :in (ui:schema table) :do
                 (ui:display-cell frame pane table column
                                  (make-instance 'row :index index :task task)
                                  (ui:view task)))
               (incf index)
               ;; (with-output-as-presentation
               ;;     (pane (make-instance 'ui:foldable-row) 'ui:foldable-row))
               )
             (when (and (not (ui:folded? task))
                        (ct:children task))
               (helper (ct:children task)))
             (when (rest tree)
               (helper (rest tree))))))
      (helper tree))))

(defun display-start-time (frame)
  (when (ui:start-time frame)
    (d:format-seconds *debug-io*
                      "~%Interface took ~M, ~S to display"
                      (- (get-universal-time)
                         (ui:start-time frame)))
    (setf (ui:start-time frame) nil)))

(defmethod ui:display-pane (frame (pane pane))
  "Display the task-duration pane."
  (unless (ui:start-time frame)
    (setf (ui:start-time frame) (get-universal-time)))
  (with-output-recording-options (pane :record t :draw nil)
    (al:if-let ((task-tree (c:task-list)))
      (formatting-table (pane)
        (let* ((history   (find-column 'past-durations-column pane))
               (timeline  (find-column 'timeline-column pane))
               (table     (table pane))
               (date      (setf (date (table pane)) (c:midnight)))
               (task-list (ct:task-tree-to-list task-tree))
               (durations-today
                 (mapcar λ(c:task-duration _ :start (ui:range-start pane)
                                             :intervals (interval-cache pane))
                         task-list))
               (top-duration (setf (max-duration-today pane)
                                   (apply #'max durations-today))))
          (setf (bar-height timeline) (text-style-height *default-text-style* pane)
                (color-index timeline) 0)
          ;; display column headers
          (formatting-row ()
            (loop :for column :in (ui:schema table)
                  :do (ui:display-cell frame pane
                                       table column
                                       (make-instance 'row :index 0)
                                       nil)))
          (display-tree task-tree frame pane table)))
      (format pane "~A~%~A"
              "Welcome to Chronometrist."
              "Use the Clock In command to begin tracking time."))
    (stream-replay pane +everywhere+)
    (display-start-time frame)))
