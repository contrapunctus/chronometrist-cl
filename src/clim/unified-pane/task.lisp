(in-package :chronometrist-clim.unified)

(defmethod ui:display-cell
    (frame (pane pane) table (column ui:task-name-column) row view)
  (let ((task (task row)))
    (formatting-cell ()
      (cond ((zerop (ui:index row))
             (present (ui:label column) 'ui:label))
            (task (indent pane row)
                  (present (ct:name task) 'ui:task-name))))))
