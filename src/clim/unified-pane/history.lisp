(in-package :chronometrist-clim.unified)

(defclass past-durations-column
    (ui:column interval-cache-mixin ui:time-range-mixin)
  ((%history-length :initarg :history-length
                    :accessor history-length
                    :initform 30
                    :documentation
                    "Number of days (as an integer) to show in history."))
  (:default-initargs :label "History"))

(defmethod initialize-instance :after
    ((column past-durations-column) &rest initargs &key &allow-other-keys)
  (setf (ui:range-start column)
        (t:adjust-timestamp (c:midnight)
          (offset :hour (* -24 (history-length column))))
        (ui:label column)
        (format nil "Past ~A days" (history-length column))))

(defmethod ui:display-cell
    (frame pane table (column past-durations-column) row view)
  (formatting-cell ()
    (if (zerop (ui:index row))
        (present (ui:label column) 'ui:label)
        (al:when-let*
            ((task (task row))
             (durations
              (loop
                :with date = (date table)
                :for i :from (history-length column) :downto 0
                :for start = (t:adjust-timestamp date (offset :day (- i)))
                :collect
                (c:task-duration task :start start
                                      :intervals (interval-cache column)))))
          (present durations 'ui:durations)))))

(define-presentation-method present
    (durations (type ui:durations) (pane pane) view &key acceptably for-context-type)
  ;; the top-duration passed as parameter is not what is required here
  (let* ((top-duration (apply #'max durations))
         (mh  (text-style-height *default-text-style* pane)) ;; bar max height
         (bw  10)) ;; bar width
    #+(or)
    (format *debug-io* "~A top-duration: ~A~%" task top-duration)
    (loop :for duration :in durations
          :for i :from 0 :do
            (let* ((x1 (* i bw))
                   (x2 (* (1+ i) bw))
                   (dp (if (zerop top-duration)
                           0
                           (round (* 100 (/ duration top-duration))))) ; duration percentage
                   ;; We invert the duration percentage by subtracting
                   ;; it from 100 - the smaller the percentage, the
                   ;; larger (= lower) we want Y2 to be.
                   (y2 (* mh (/ (- 100 dp) 100))))
              #+(or)
              (format *debug-io* "  duration: ~A mh: ~A dp: ~A y2: ~A~%"
                      duration mh x2 dp (round y2))
              (draw-rectangle* pane x1 mh x2 (round y2))))))
