(in-package :chronometrist-clim.unified)
(nr:in-readtable chronometrist:readtable)

;; It seems it's not possible for us to know how much screen width we
;; have left for a table column to use, such that we might draw the
;; cell contents accordingly...
(defclass timeline-column (ui:column)
  ((%left-gap :initarg :left-gap
              :initform 2
              :accessor left-gap
              :documentation
              "Horizontal distance between graph and window edge.")
   (%pane-width :initarg :drawing-width
                :initform nil
                :accessor pane-width
                :documentation "Width of drawing region.")
   (%ruler-width :initarg :ruler-width
                 :initform nil
                 :accessor ruler-width)
   (%mark-gap :initarg :mark-gap
              :initform nil
              :accessor mark-gap
              :documentation
              "Horizontal distance between each mark of the chart's ruler.")
   (%mark-height :initarg :mark-height
                 :initform 15
                 :accessor mark-height
                 :documentation
                 "Height of each mark of the chart's ruler.")
   (%bar-height :initarg :bar-height
                :initform 30
                :accessor bar-height
                :documentation "Height of each bar.")
   (%color-index :initarg :color-index
                 :accessor color-index
                 :initform 0))
  (:default-initargs :label "Intervals today"))

(defmethod initialize-instance :after
    ((timeline timeline-column) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (unless (pane-width timeline)
    (setf (pane-width timeline) (graft-width (find-graft))))
  (let ((pw (pane-width timeline)))
    (unless (mark-gap timeline)
      ;; There are 24 hours in the day, but we don't want exactly 24
      ;; equal parts of the width - we want slightly less, so we have
      ;; space to display "24:00" next to the last mark.
      (setf (mark-gap timeline) (- (/ pw 24 1.5) 2)))
    (unless (ruler-width timeline)
      (setf (ruler-width timeline) (- pw 2)))))

(defun draw-ruler (column pane)
  (loop :with df = (ui:date-format pane)
        :with tf = (ui:time-format pane)
        :with rs = (ui:range-start pane)
        :with rw = (ruler-width column)
        :with mh = (mark-height column)
        :with mg = (mark-gap column)
        :with lg = (left-gap column)
          :initially
             ;; (draw-text* pane (c:format-timestring rs df) 0 0)
             (draw-line* pane 2 2 rw 2)
        :for index :from 0 :to 24
        :for time = (c:format-timestring (t:adjust-timestamp rs
                                           (offset :hour index))
                                         tf)
        ;; Horizontal position of ruler mark
        :for x = lg :then (+ lg (* index mg))
        ;; Draw the mark and the hour
        :do (draw-line* pane x lg x mh)
            (draw-text* pane time
                        (+ 2 x) ;; Horizontal gap between ruler mark and hour text
                        mh)))

(defmethod ui:display-cell
    (frame pane table (column timeline-column) row view)
  (let* ((start (ui:range-start pane))
         (end   (ui:range-end pane))
         (task  (task row)))
    (formatting-cell ()
      (cond ((zerop (ui:index row))
             (formatting-table ()
               (formatting-column ()
                 (formatting-cell ()
                   (present (ui:label column) 'ui:label))
                 (formatting-cell ()
                   (draw-ruler column pane)))))
            (task
             (let ((intervals (remove-if-not λ(ct:interval-match? task _)
                                             (al:flatten
                                              (multiple-value-list
                                               (c:intervals (c:active-backend)
                                                            :start start :end end))))))
               (present intervals 'ui:intervals)
               (when intervals
                 (incf (color-index column)))))))))

(define-presentation-method present
    (object (type ui:intervals) (stream pane) view
            &key acceptably for-context-type)
  (let* ((column (find-column 'timeline-column stream))
         (lg     (left-gap column))
         (rw     (ruler-width column))
         ;; Range start and end as UNIX epoch times
         (rs     (ui:range-start stream))
         (re     (ui:range-end stream))
         (rsu    (t:timestamp-to-unix rs))
         (reu    (t:timestamp-to-unix re))
         ;; The range of time displayed in the column, as seconds
         (range      (- reu rsu))
         (bar-vstart 20)
         (bar-height (+ (bar-height column) bar-vstart)) ; bar height
         (trim-end   (if (t:timestamp< (t:now) re)
                         (t:now)
                         re))
         (trimmed    (c:trim-intervals object rs trim-end))
         (color      (elt (at:bar-background-inks)
                          (color-index column))))
    (loop :for interval :in trimmed
          :for i :in object
          ;; Determine horizontal position for start of bar, based on interval start time
          :for isu = (t:timestamp-to-unix (c:start interval)) ; interval start UNIX
          :for isf = (/ (- isu rsu) range) ; interval start fraction
          :for bar-hstart = (+ lg (* rw isf))
          ;; Determine end of bar based on interval end time.
          ;; Active intervals need a stop time to display a bar, but
          ;; updating the `stop' slot would disturb the object being
          ;; presented (it would become an inactive interval). So we
          ;; determine the stop time non-destructively.
          :for ie = (or (c:stop interval) (active-interval-stop re)) ; interval end
          :for ieu = (t:timestamp-to-unix ie) ; interval end UNIX
          :for ief = (/ (- ieu rsu) range)    ; interval end fraction
          :for bar-width = (+ lg (* rw ief))
          ;; Draw filled box, an outline, and interval text
          :do #+(or)
              (let ((s *debug-io*))
                (format s "~%name: ~A~%" (c:name interval))
                (format s "isu: ~A (~A)~%" isu
                        (t:format-timestring nil (c:start interval)))
                (format s "rsu: ~A (~A)~%" rsu
                        (t:format-timestring nil rs))
                (format s "ruler width: ~A~%" rw)
                (format s "isf: ~A~%" isf)
                (format s "bar-hstart: ~A~%" bar-hstart))
              ;; Hack to prevent :align-x :left from moving the first
              ;; bar for a task from its correct position to 00:00
              (draw-point* stream 0 bar-vstart :ink +transparent-ink+)
              (with-output-as-presentation (stream i 'c:interval)
                (draw-rectangle* stream
                                 bar-hstart bar-vstart bar-width bar-height
                                 :ink color)
                (draw-rectangle* stream
                                 bar-hstart bar-vstart bar-width bar-height
                                 :ink +black+ :filled nil
                                 ;; :line-thickness 2
                                 :line-joint-shape :bevel)))))
