(in-package :cl)

(defpackage :chronometrist-clim.unified
  (:use :clim :clim-lisp)
  (:shadow #:pane)
  (:local-nicknames (:c  :chronometrist)
                    (:ct :chronometrist.task)
                    (:ui :chronometrist-clim)
                    (:at :anathema)
                    (:ats :anathema.style)
                    (:al :alexandria)
                    (:t  :local-time)
                    (:d :format-seconds)
                    (:nr :named-readtables))
  (:export #:task #:pane))

(trivial-package-local-nicknames:add-package-local-nickname
 :cu :chronometrist-clim.unified :chronometrist-clim)
