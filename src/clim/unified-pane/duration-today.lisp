(in-package :chronometrist-clim.unified)

(defclass duration-today-column (ui:column) ()
  (:default-initargs :label "Today")
  (:documentation "A column displaying the time tracked for a task today."))

;; Currently, bar height is relative to the largest duration in the
;; chart. Other alternatives could be to make them relative to the
;; length of the day, or the goal.
(defmethod ui:display-cell
    (frame pane table (column duration-today-column) row view)
  (formatting-cell ()
    (cond
      ((zerop (ui:index row))
       (present (ui:label column) 'ui:label))
      (t (al:when-let*
             ((task (task row))
              (duration
               (c:task-duration task :start (ui:range-start pane)
                                     :intervals (interval-cache pane))))
           ;; (format *debug-io* "~S ~A~%" task duration)
           (present duration 'ui:duration))))))

(define-presentation-method present
    (duration (type ui:duration) (pane pane) view &key acceptably for-context-type)
  (let ((top-duration (max-duration-today pane)))
    (cond ((or (zerop duration)
               (zerop top-duration))
           (format t "~10@A" "-"))
          (t (let* ((bh  (text-style-height *default-text-style* pane)) ;; bar height
                    (bl  (* (/ duration top-duration) 100)) ; bar length
                    (duration-string (d:format-seconds
                                      nil (ui:duration-format pane) duration)))
               (draw-rectangle* pane 0 0 bl bh)
               (draw-text* pane duration-string 0 13
                           :ink +black+
                           ;; :text-style (make-text-style nil :bold nil)
                           ))))))
