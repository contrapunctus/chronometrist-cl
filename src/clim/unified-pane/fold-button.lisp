(in-package :chronometrist-clim.unified)

(defclass fold-button-column (ui:column)
  ((%width :initarg :width
           :accessor width
           :initform 6))
  (:documentation "A column displaying buttons to fold/unfold rows.")
  (:default-initargs :label ""))

(defclass fold-button-cell (ui:cell ui:label-mixin) ())

(defmethod ui:display-cell
    (frame (pane pane) table (column fold-button-column) row view)
  (let ((task (task row)))
    (formatting-cell ()
      (cond ((or (zerop (ui:index row))
                 (and task
                      (not (ct:children task))))
             (terpri pane))
            (task
             (let* ((tw (width column)) ; triangle width
                    (iw (+ 0 (* tw (1+ (ct:depth (task row))))))) ; indent width
               ;; (format *debug-io* "task: ~A iw: ~A~%" (c:name (task row)) iw)
               (draw-rectangle* pane 0 0 iw 5 :ink +transparent-ink+)
               ;; (indent pane row)
               (with-output-as-presentation (pane task 'ct:task)
                 (at:with-style (pane ats:*link*)
                   (if (ui:folded? task)
                       ;; right-facing triangle
                       (draw-polygon* pane (list iw 0
                                                 iw (* tw 2)
                                                 (+ iw tw) tw)
                                      :closed t)
                       ;; downward-facing triangle
                       (draw-polygon* pane (list iw 0
                                                 (+ iw (* tw 2)) 0
                                                 (+ iw tw) tw)
                                      :closed t))))))))))
