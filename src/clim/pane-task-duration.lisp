(in-package :chronometrist-clim.task-duration)

(defclass table (ui:table) ()
  (:default-initargs
   :schema
   (mapcar #'make-instance
           '(ui:index
             ui:task
             task-duration))))

(defclass row (foldable-row) ())

(defclass pane (ui:pane)
  ((%table :initarg :table
           :accessor table
           :initform (make-instance 'table)))
  (:documentation
   "A pane displaying a list of tasks and the time spent on them today."))

(defmethod ui:display-cell
    (table (column ui:task) (index integer)
     frame (pane pane) &key task date &allow-other-keys)
  (with-output-as-presentation (pane task 'task-name)
    (format t "~A" task))
  task)

(defclass task-duration (ui:cell) ()
  (:default-initargs :label "Time"))

(defmethod ui:display-cell
    (table (column task-duration) (index integer) frame pane
     &key task date &allow-other-keys)
  (let ((task-duration (c:task-duration task :start date)))
    (with-output-as-presentation (pane task-duration 'number)
      (if (zerop task-duration)
          (format t "~10@A" "-")
          (format-seconds t (duration-format pane) task-duration)))
    task-duration))

(defmethod ui:display-pane (frame (pane pane))
  "Display the task-duration pane."
  (if (task-list)
      (formatting-table (pane)
        (loop
          :with date   = (today)
          :with table  = (table pane)
          :with schema = (schema table)
          :for task :in (task-list)
          ;; :for row :in (table-rows table frame pane)
          :for index :from 0 :do
            (formatting-row (pane)
              ;; (with-output-as-presentation (t row 'foldable-row))
              (if (zerop index)
                  (loop :for string :in (mapcar #'ui:label schema)
                        :do (formatting-cell (pane)
                              (format t "~A" string)))
                  (loop :for column :in schema :do
                    (formatting-cell (pane)
                      (ui:display-cell table column index data frame pane
                                    :date date :task task)))))))
      (format pane "~A~%~A"
              "Welcome to Chronometrist."
              "Use the Clock In command to begin tracking time.")))
