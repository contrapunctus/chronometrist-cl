(in-package :chronometrist-clim)

(define-presentation-translator interval-to-task (interval string chronometrist)
    (interval)
  ;; XXX
  (c:names interval))

(define-chronometrist-command (com-clock-in :name t :menu t) ((task 'string))
  (c:clock-in (c:active-backend) task)
  (update-interval-command-state))

(defun update-interval-command-state ()
  (r:bind ((backend  (c:active-backend))
           (_ active (c:intervals backend)))
    (setf (command-enabled 'com-clock-out *application-frame*)
          (or active)
          (command-enabled 'com-discard-active *application-frame*)
          (or active))))

(define-chronometrist-command (com-clock-out :name t :menu t)
    ((interval '(interval t))) ;; Accept only active intervals
  (c:clock-out (c:active-backend) interval)
  ;; If there are no more active intervals, disable clock-out and discard-active
  (update-interval-command-state))

(define-chronometrist-command (com-discard-active :name t :menu t) ()
  (r:bind ((backend  (c:active-backend))
           (_ active-intervals (intervals backend)))
    (pat:match active-intervals
      ((satisfies null))
      ((list interval)
       (remove (c:active-backend) interval))
      (t (accept '(interval t))))
    (update-interval-command-state)))

(define-chronometrist-command (com-delete :name t :menu t) ((interval 'c:interval))
  (c:remove (c:active-backend) interval)
  (update-interval-command-state))

(define-presentation-to-command-translator clock-in
    (task-name com-clock-in chronometrist) (task)
  (list task))
