(in-package :chronometrist-clim)

(define-chronometrist-command (com-cycle-row :name t :menu t) ((task 'ct:task))
  (setf (folded? task) (not (folded? task))))

(define-presentation-to-command-translator click-button-to-cycle
    (ct:task com-cycle-row chronometrist) (task)
  (list task))

(define-chronometrist-command (com-cycle-all :name t :menu t) ()
  (let* ((task-list (ct:task-tree-to-list (c:task-list)))
         ;; If any tasks are unfolded, fold all tasks; otherwise, unfold all tasks.
         (unfolded  (remove-if #'folded? task-list)))
    (loop :for task :in task-list :do
      (setf (folded? task) unfolded))))
