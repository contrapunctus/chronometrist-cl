(in-package :chronometrist-clim)

(define-chronometrist-command (com-refresh :name t :menu t) ())

(define-chronometrist-command (com-cycle-layout :name t :menu t) ()
  (flet ((find-index (item seq pred)
           (loop :for elt :in seq
                 :for i :from 0
                 :when (apply pred item elt nil)
                   :return i)))
    (let* ((layouts (apply #'al:circular-list (frame-all-layouts *application-frame*)))
           (current (frame-current-layout *application-frame*))
           (index   (find-index current layouts #'eq)))
      (setf (frame-current-layout *application-frame*) (elt layouts (1+ index))))))
