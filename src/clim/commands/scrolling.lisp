(in-package :chronometrist-clim)

;; (define-gesture-name :scroll-previous :pointer-scroll (:wheel-up))
;; (define-gesture-name :scroll-next :pointer-scroll (:wheel-down))

;; (define-presentation-to-command-translator scroll-back
;;     (blank-area com-previous chronometrist :gesture :scroll-previous) (blank-area)
;;   (list))

;; (define-presentation-to-command-translator scroll-forward
;;     (blank-area com-next chronometrist :gesture :scroll-next) (blank-area)
;;   (list))

(defun pane-range (pane)
  (let* ((rs (range-start pane))
         (re (range-end pane)))
    (t:timestamp-difference re rs)))

(defun update-pane-range (pane offset)
  (let ((rs (range-start pane))
        (re (range-end pane)))
    (setf (range-start pane) (t:adjust-timestamp rs (offset :sec offset))
          (range-end pane) (t:adjust-timestamp re (offset :sec offset)))))

(define-chronometrist-command (com-next :name t :menu t) ()
  (let* ((pane   (unified-pane *application-frame*))
         (offset (pane-range pane)))
    (update-pane-range pane offset)
    (update-com-next-state pane)))

(define-chronometrist-command (com-previous :name t :menu t) ()
  (let* ((pane   (unified-pane *application-frame*))
         (offset (- (pane-range pane))))
    (update-pane-range pane offset)
    (update-com-next-state pane)))
