(in-package :chronometrist-clim)

;; When adding a new instance of `task', we don't know in advance what
;; type of `task' the user wants - they select it in the editing UI,
;; but in order to show that editing UI, we need to insert a `task' in
;; the task tree. Hence this placeholder class.
(defclass new-task-placeholder (ct:task) ()
  (:default-initargs :name "Unnamed task"))

(defmethod display-cell
    (frame (pane cu:pane) table (column task-name-column) row (view edit-view))
  (let ((fg (at:style-foreground-ink ats:*default*))
        (bg (at:style-background-ink ats:*default*)))
    (formatting-cell (pane)
      (format pane "Name  ")
      (surrounding-output-with-border (pane :padding 2 :shape :rounded)
        (with-output-as-gadget (pane)
          (make-pane 'text-field-pane
                     :value (ct:name (cu:task row))
                     :foreground fg
                     :background bg))))))

(define-chronometrist-command (com-add-task :name t :menu t) ()
  ;; when ≥1 tasks - ask for place
  ;; insert placeholder
  (let* ((place (when (c:task-list)
                  ))
         (view (make-instance 'edit-view))
         (task (make-instance 'new-task-placeholder :view view)))
    (push task c:*tasks*)
    #+(or)
    (if place
        ;; ...
        (push task (c:task-list)))))

(define-chronometrist-command (com-remove-task :name t :menu t) ((task 'ct:task))
  )
