(in-package :chronometrist-clim)

(defclass table ()
  ((%schema
    :initarg :schema :accessor schema :initform nil
    :documentation
    "The columns of this pane, as a list of `column' instances.")
   (%data :initarg :data
          :accessor data
          :documentation
          "The data in this table, as a list of lists."))
  (:documentation
   "A class for tables which are easy to extend with new columns."))

(defclass nest-mixin ()
  ((%children :initarg :children
              :accessor children)
   (%depth :initarg :depth
           :accessor depth)))

(defclass label-mixin ()
  ((%label :initarg :label
           :type string
           :accessor label)))

(define-presentation-type label () :inherit-from 'string)

(defclass index-mixin ()
  ((%index :initarg :index
           :accessor index
           :type integer)))

(defclass row (index-mixin) ())
(defclass foldable-row (fold-mixin row) ())

(defclass column (label-mixin) ())

(defclass index-column (column index-mixin) ()
  (:default-initargs :label "#"))

(defclass task-name-column (column) ()
  (:default-initargs :label "Task"))

(defgeneric display-cell (frame pane table column row view)
  (:documentation
   "Display and return the contents of a cell in TABLE.

FRAME and PANE are the CLIM frame and pane as passed to the display
function.

TABLE, ROW, and COLUMN are instances of `table', `row', and `column',
respectively."))

(defclass cell (label-mixin) ())
(defclass header (cell) ())
(defclass index (cell index-mixin) ())

(defmethod display-cell
    (frame pane (table table) (column index-column) row view)
  (let ((cell (make-instance 'index :index (index row)
                                    :label (label column))))
    (formatting-cell ()
      (present cell 'index))))

(define-presentation-method present
    (object (type index) stream view &key acceptably for-context-type)
  (if (zerop (index object))
      (format stream "~A" (label object))
      (format stream "~2@A" (index object))))
