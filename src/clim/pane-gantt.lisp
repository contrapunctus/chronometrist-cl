(in-package :chronometrist-clim.gantt)

(defclass pane (chronometrist-clim:pane) ()
  (:default-initargs
   :display-function 'chronometrist-clim:display-pane
   :time-format '((:hour 2) ":" (:min 2))
   :incremental-redisplay nil)
  (:documentation
   "A pane displaying a graphical log of intervals and events in a
   time range (default - today)."))

(defun active-interval-stop (range-end)
  "Return (as a `local-time:timestamp') RANGE-END or the current time,
based on which is earlier."
  (if (timestamp>= range-end (t:now))
      (t:now)
      range-end))

(defclass chart ()
  ((%pane :initarg :pane
          :initform (error "No pane provided")
          :accessor pane
          :documentation "The pane to draw to.")
   (%left-gap :initarg :left-gap
              :initform 2
              :accessor left-gap
              :documentation
              "Horizontal distance between graph and window edge.")
   (%pane-width :initarg :drawing-width
                :initform nil
                :accessor pane-width
                :documentation "Width of drawing region.")
   (%ruler-width :initarg :ruler-width
                 :initform nil
                 :accessor ruler-width)
   (%mark-gap :initarg :mark-gap
              :initform nil
              :accessor mark-gap
              :documentation
              "Horizontal distance between each mark of the chart's ruler.")
   (%mark-height :initarg :mark-height
                 :initform 15
                 :accessor mark-height
                 :documentation
                 "Height of each mark of the chart's ruler.")
   (%bar-height :initarg :bar-height
                :initform 30
                :accessor bar-height
                :documentation "Height of each bar.")
   (%bar-background-inks
    :initarg :bar-background-inks
    :accessor bar-background-inks
    :initform (anathema:bar-background-inks)
    :documentation
    "Circular association list of background and text colors used for bars.")))

(defmethod initialize-instance :after
    ((chart chart) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (unless (pane-width chart)
    (setf (pane-width chart)
          (rectangle-width (bounding-rectangle (sheet-region (pane chart))))))
  (let ((pw (pane-width chart)))
    (unless (mark-gap chart)
      ;; There are 24 hours in the day, but we don't want exactly 24
      ;; equal parts of the width - we want slightly less, so we have
      ;; space to display "24:00" next to the last mark.
      (setf (mark-gap chart) (- (/ pw 24) 2)))
    (unless (ruler-width chart)
      (setf (ruler-width chart) (- pw 2)))))

(defun draw-ruler (chart)
  "Draw a ruler in PANE.
PANE should be an instance of `chronometrist-clim.gantt:pane'."
  (loop :with pane = (pane chart)
        :with df = (ui:date-format pane)
        :with tf = (ui:time-format pane)
        :with rs = (ui:range-start (pane chart))
        :with rw = (ruler-width chart)
        :with mh = (mark-height chart)
        :with mg = (mark-gap chart)
        :with lg = (left-gap chart)
          :initially
             (draw-text* pane (c:format-timestring rs df) 0 0)
             (draw-line* pane 2 2 rw 2)
        :for index :from 0 to 24
        :for time = (c:format-timestring (t:adjust-timestamp rs
                                           (offset :hour index))
                                         tf)
        ;; Horizontal position of ruler mark
        :for x = lg :then (+ lg (* index mg))
        ;; Draw the mark and the hour
        :do (draw-line* pane x lg x mh)
            (draw-text* pane time
                        (+ 2 x) ;; Horizontal gap between ruler mark and hour text
                        mh)))

(defun draw-bars (chart inactive active)
  "Draw bars for INACTIVE and ACTIVE intervals in PANE.

INACTIVE and ACTIVE should be lists of `chronometrist:interval' instances.

PANE should be an instance of `chronometrist-clim.gantt:pane'."
  (let* ((pane (pane chart))
         (lg   (left-gap chart))
         (rw   (ruler-width chart))
         ;; Range start and end as UNIX epoch times
         (rs   (ui:range-start pane))
         (re   (ui:range-end pane))
         (rsu  (t:timestamp-to-unix rs))
         (reu  (t:timestamp-to-unix re))
         ;; The range of time displayed in the chart, as seconds
         (range      (- reu rsu))
         (bar-vstart 20)
         (bar-height (+ (bar-height chart) bar-vstart))
         (intervals  (append inactive active))
         (trimmed    (c:trim-intervals intervals rs
                                       (if (t:timestamp< (t:now) re)
                                           (t:now)
                                           re))))
    (loop :for interval :in trimmed
          :for i :in intervals
          :for color :in (bar-background-inks chart)
          ;; Determine horizontal position for start of bar, based on interval start time
          :for su = (t:timestamp-to-unix (c:start interval)) ; start [time as] UNIX [epoch time]
          :for sp = (* 100 (/ (- su rsu) range)) ; start percentage
          :for bar-hstart = (+ lg (* rw (/ sp 100)))
          ;; Determine end of bar based on interval end time.
          ;; Active intervals need a stop time to display a bar, but
          ;; updating the stop slot would disturb the object being
          ;; presented (it would become an inactive interval). So we
          ;; determine the stop time non-destructively.
          :for ie = (or (c:stop interval) (active-interval-stop re)) ; interval end
          :for ieu = (t:timestamp-to-unix ie)       ; interval end UNIX
          :for ep = (* 100 (/ (- ieu rsu) range)) ; end percentage
          :for bar-width  = (+ lg (* rw (/ ep 100)))
          ;; Draw filled box, an outline, and interval text
          :do #-(and) (format *debug-io*
                              "~%name: ~A~%ruler-width: ~A~%bar-width: ~A~%bar-hstart: ~A~%"
                              (name interval) rw bar-width bar-hstart)
              (with-output-as-presentation (pane i 'c:interval)
                (draw-rectangle* pane
                                 bar-hstart bar-vstart
                                 bar-width bar-height
                                 :ink color)
                (draw-rectangle* pane
                                 bar-hstart bar-vstart
                                 bar-width bar-height
                                 :ink +black+
                                 :filled nil
                                 ;; :line-thickness 2
                                 :line-joint-shape :bevel)
                (draw-text* pane (c:names interval)
                            (+ 5 bar-hstart) (+ 20 bar-vstart)
                            :ink (at:style-foreground-ink ats:*graph-bar*))))))

(defmethod chronometrist-clim:display-pane (frame (pane pane))
  (r:bind ((start           (ui:range-start pane))
           (end             (ui:range-end pane))
           (inactive active (c:intervals (c:active-backend) :start start :end end))
           (today-start     (c:midnight))
           (today-end       (t:adjust-timestamp today-start (offset :hour 24)))
           (chart (make-instance 'chart :pane pane)))
    (draw-ruler chart)
    ;; For today or past days, show the intervals
    (when (or inactive
              (and active
                   (t:timestamp>= end today-end))) ;; not exactly exhaustive
      (draw-bars chart inactive active))))
