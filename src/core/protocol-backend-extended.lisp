(in-package :chronometrist)

(defgeneric list-intervals (backend)
  (:documentation
   "Return all intervals from BACKEND.")
  (:method (backend)
    (multiple-value-bind (inactive active) (intervals backend)
      (append active inactive))))

(defgeneric list-tasks (backend)
  (:documentation
   "Return all tasks recorded in BACKEND as a list of `chronometrist:task' instances.")
  (:method (backend)
    (s:~> (list-intervals backend)
          (mapcar #'names _)
          (al:flatten _)
          (remove-duplicates _ :test #'equal)
          (sort _ #'string<)
          (mapcar #'chronometrist.task:make-simple-task _))))

(defgeneric active-days (backend task &key start end)
  (:documentation "From BACKEND, return number of days on which TASK had recorded time."))

(defgeneric count-records (backend)
  (:documentation "Return number of records in BACKEND."))

(defun current-task (&optional (backend (active-backend)))
  "Return the name of the active task as a string, or nil if not clocked in."
  (let ((interval (latest-interval backend)))
    (if (stop interval)
        nil
        (name interval))))

(defgeneric clock-in (backend task)
  (:documentation "Insert a new active interval for TASK to BACKEND.")
  (:method ((backend backend) (task chronometrist.task:task))
    (insert backend (chronometrist.task:task-interval task))))

(defgeneric clock-out (backend interval)
  (:documentation "In BACKEND, clock out of INTERVAL.")
  (:method ((backend backend) (interval interval))
    (let ((new (copy-object interval)))
      (setf (stop new) (now))
      (update backend interval new))))

(defgeneric latest-interval (backend)
  (:documentation "Return the latest interval from BACKEND, or nil if BACKEND contains no intervals.

Return value may be active, i.e. it may or may not have a `stop' value.

If the latest record starts on one day and ends on another, the
entire (unsplit) record must be returned."))

(defgeneric task-records-for-date (backend task date &key &allow-other-keys)
  (:documentation "From BACKEND, return records for TASK on DATE as a list of plists.

DATE must be an instance of `local-time:timestamp'.

Return nil if BACKEND contains no records.")
  (:method ((backend chronometrist:backend) (task string) (date timestamp)
            &key &allow-other-keys)
    (let ((end (adjust-timestamp date (offset :hour 24))))
      (loop for interval in (intervals backend :start date :end end)
            when (equal task (name interval))
              collect interval))))
