(in-package :chronometrist)

(defclass option () ()
  (:documentation "Protocol class for all configuration options."))

(defclass global-option (option) ()
  (:documentation
   "Protocol class for options which have one value for the entire application."))

(defclass task-option (option) ()
  (:documentation
   "Protocol class for options which can have different values for each task."))

(defgeneric global-value (option)
  (:documentation "Return the configuration value for OPTION.
OPTION must be a keyword."))

(defgeneric (setf global-value) (new-value option)
  (:documentation
   "Set the value for configuration OPTION to NEW-VALUE.
OPTION must be a keyword."))

(defgeneric task-value (task option)
  (:documentation
   "Return the TASK-specific configuration value for OPTION.

TASK must be a string. OPTION must be a keyword.

If a TASK-specific value is not found, return a default value.

The secondary value is T if the task-specific value was provided, and
NIL if the default was used."))

(defgeneric (setf task-value) (new-value task keyword)
  (:documentation
   "Set the TASK-specific configuration value for OPTION to NEW-VALUE.

TASK must be a string. OPTION must be a keyword."))

(defmacro define-global-option (name default &key documentation (value-class t))
  "Define an application-wide configuration option called NAME.

The values for this option must be instances of CLASS.

DEFAULT is the default value for this option."
  (let ((keyword (gensym)))
    `(let ((,keyword (al:make-keyword ',name)))
       (defclass ,name (global-option) ()
         (:documentation ,documentation))
       (defvar ,(earmuff-symbol name)
         (make-instance ',name)
         ,documentation)
       (defmethod global-value ((option ,name))
         (conf:value :global ,keyword))
       (defmethod (setf global-value) ((new-value ,value-class) (option ,name))
         (setf (conf:value :global ,keyword) new-value))
       (conf:defaulted-value ,default :global ,keyword))))

(defmacro define-task-option (name default &key documentation (value-class t))
  "Define a task-specific configuration option called NAME.

The values for this option must be instances of VALUE-CLASS.

DEFAULT is the default value for this option."
  (let ((keyword (gensym)))
    `(let ((,keyword (al:make-keyword ',name)))
       (defclass ,name (task-option) ()
         (:documentation ,documentation))
       (defvar ,(earmuff-symbol name)
         (make-instance ',name)
         ,documentation)
       (defmethod task-value ((task string) (option ,name))
         (multiple-value-bind (value supplied-p) (conf:value :task task ,keyword)
           (values (or value (conf:value :task :default ,keyword))
                   supplied-p)))
       (defmethod (setf task-value) ((new-value ,value-class) (task string) (option ,name))
         (setf (conf:value :task task ,keyword) new-value))
       (conf:defaulted-value ,default :task :default ,keyword))))

(defun load-configuration ()
  (conf:restore (merge-pathnames "chronometrist.conf" (uiop:xdg-config-home))))

(eval-when (:load-toplevel)
  (load-configuration))

(define-global-option database-directory
    (ensure-directories-exist
     (merge-pathnames "chronometrist/" (uiop:xdg-data-home)))
  :documentation "Absolute pathname for the database directory."
  :value-class pathname)

(define-global-option database-pathname-name "data"
  :documentation "Base name for the database file."
  :value-class string)

(define-global-option init-pathname
    (merge-pathnames
     "init.lisp"
     (ensure-directories-exist
      (merge-pathnames "chronometrist/" (uiop:xdg-config-home))))
  :documentation "Absolute pathname for the user's init file."
  :value-class pathname)

(define-global-option allow-concurrent-intervals t
  :documentation "Whether to allow the creation of concurrent intervals.")

(define-global-option day-start-time 0
  :documentation
  "Time at which a day is considered to start, as number of seconds from midnight."
  :value-class integer)

(define-global-option week-start-day 0
  :documentation
  "Day on which the week is considered to start, as an integer between 0-6."
  :value-class integer)

(define-task-option show t
  :documentation "Whether to hide or show this task in the application.")

(define-task-option goal nil
  :documentation
  "The daily time goal for this task, in seconds.")

(define-task-option property-presets nil
  :documentation
  "Preset property-value combinations for this task, to be used as suggestions.")
