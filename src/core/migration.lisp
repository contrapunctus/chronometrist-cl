(in-package :chronometrist)

(defun remove-prefix (string)
  (replace-regexp-in-string "^" "" string))
