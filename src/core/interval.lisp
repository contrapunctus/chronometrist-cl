(in-package :chronometrist)

(defclass entry ()
  ((%names
    :initarg :names
    :accessor names
    :documentation
    "Names describing the activity that occurred in this entry, as a list of strings.")
   (%properties :initarg :properties
                :initform nil
                :accessor properties))
  (:documentation
   "Protocol class for entries in the Chronometrist database."))

(defclass interval (entry)
  ((%start
    :initarg :start
    :initform nil
    :accessor start
    :type timestamp
    :documentation
    "The time at which this interval started, as an instance of `local-time:timestamp'.")
   (%stop
    :initarg :stop
    :initform nil
    :accessor stop
    :type (or timestamp null)
    :documentation
    "The time at which this interval ended, as an instance of `local-time:timestamp'."))
  (:documentation
   "A time range spent on a specific task, with optional properties."))

(defun make-interval (names start &optional stop properties)
  (make-instance 'chronometrist:interval
                 :names names
                 :start start
                 :stop stop
                 :properties
                 (cond ((stringp properties)
                        (read-from-string properties))
                       ((listp properties)
                        properties)
                       (t (error "Invalid value for properties")))))

(defun interval-duration (interval)
  (t:timestamp-difference (stop interval) (start interval)))

(defmethod print-object ((interval interval) stream)
  (print-unreadable-object (interval stream :type t)
    (let ((format '((:year 4) "-" (:month 2) "-" (:day 2) " "
                    (:hour 2) ":" (:min 2) ":" (:sec 2) :gmt-offset-or-z)))
      (format stream "~S  ~A~@[  ~A~]"
              (names interval)
              (format-timestring (start interval) format)
              (and (stop interval)
                   (format-timestring (stop interval) format))))))

(defmethod copy-object ((interval interval))
  (make-instance 'interval
                 :properties (or (properties interval))
                 :names (or (names interval))
                 :start (or (start interval))
                 :stop  (or (stop interval))))

(defun interval-equal? (a b)
  (and (equal (properties a) (properties b))
       (equal (names a) (names b))
       (or (equal (start a) (start b))
           (timestamp= (start a) (start b)))
       (or (equal (stop a) (stop b))
           (timestamp= (stop a) (stop b)))))

(defun split-interval
    (interval &optional (day-start-time (global-value *day-start-time*)))
  "Return a list of two intervals if INTERVAL spans a midnight, else nil."
  (when (stop interval)
    (let ((split-time (split-time (start interval)
                                  (stop interval)
                                  day-start-time)))
      (when split-time
        (let-match* (((plist :start start-1 :stop stop-1) (first  split-time))
                     ((plist :start start-2 :stop stop-2) (second split-time))
                     (interval-1 (copy-object interval))
                     (interval-2 (copy-object interval)))
          (setf (start interval-1) start-1
                (stop  interval-1) stop-1
                (start interval-2) start-2
                (stop  interval-2) stop-2)
          (list interval-1 interval-2))))))

(defun trim-intervals (intervals start end)
  "Return INTERVALS with timestamps changed to fall between START and END."
  (loop :for i :in intervals
        :collect
        (let ((i (copy-object i)))
          (when (t:timestamp< (start i) start)
            (setf (start i) start))
          (when (or (not (stop i))
                    (t:timestamp< end (stop i)))
            (setf (stop i) end))
          i)))

(defun intervals-split-p (old new)
  "Return t if intervals OLD and NEW are split.

Split intervals means that the `stop' time of OLD must be the
same as the `start' time of NEW, and they must have identical
properties (except `start' and `stop')."
  (let ((old-stop (stop old)))
    (and old-stop
         (timestamp= old-stop
                     (start new))
         (equal (names old)
                (names new))
         (equal (properties old)
                (properties new)))))

(defun interval-unify (old new)
  "Return a single interval which has the `start' time of OLD and the `stop' time of NEW.

OLD and NEW must be instances of `chronometrist:interval'."
  (when (intervals-split-p old new)
    (make-instance 'interval
                   :names (names old)
                   :start (start old)
                   :stop  (stop new)
                   :properties (properties old))))

(defun interval-in-range? (interval range-start range-end)
  "Return non-nil if interval starts or ends between RANGE-START and RANGE-END.

Return value is :INACTIVE if interval is inactive, :ACTIVE if interval
is active, and nil if interval does not start or end between
RANGE-START and RANGE-END."
  (let ((start (start interval))
        (stop  (stop interval)))
    (cond ((or ;; inactive intervals which start within the range
            (and (t:timestamp>= start range-start)
                 (t:timestamp<= start range-end)
                 stop)
            ;; inactive intervals which end within the range
            (and stop
                 (t:timestamp>= stop range-start)
                 (t:timestamp<= stop range-end)))
           :inactive)
          ((and (t:timestamp<= start range-end)
                (null stop))
           :active)
          (t nil))))

(defclass event (entry)
  ((%time :initarg :time
          :accessor event-time
          :type integer
          :documentation "The time at which this interval started, as
         an integer representing the UNIX epoch time."))
  (:documentation "A named timestamp with optional properties."))
