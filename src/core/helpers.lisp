(in-package :chronometrist)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (nr:defreadtable readtable
    (:merge :standard r:rutils-readtable fn:fn-reader))

  (defun earmuff-symbol (symbol)
    (intern (concatenate 'string "*" (symbol-name symbol) "*"))))

(defun make-hash-table-1 ()
  "Return an empty hash table with `equal' as test."
  (make-hash-table :test #'equal))

(defun plist-remove (plist &rest keys)
  "Return PLIST with KEYS and their associated values removed."
  (loop for key in keys do (remf plist key))
  plist)

(defun plist-key-values (plist)
  "Return user key-values from PLIST."
  (plist-remove plist :name :tags :start :stop))

(defun plist-p (list)
  "Return non-nil if LIST is a property list, i.e. (:KEYWORD VALUE ...)"
  (when list
    (while (consp list)
      (setq list (if (and (keywordp (cl-first list)) (consp (cl-rest list)))
                     (cddr list)
                   'not-plist)))
    (null list)))

(defun ht-update (plist hash-table &optional replace)
  "Return HASH-TABLE with PLIST added as the latest interval.
If REPLACE is non-nil, replace the last interval with PLIST."
  (let* ((date (->> (getf plist :start)
                    (parse-timestring )
                    (ts-format "%F" )))
         (events-today (gethash date hash-table)))
    (--> (if replace (-drop-last 1 events-today) events-today)
         (append it (list plist))
         (puthash date it hash-table))
    hash-table))

(defun ht-last-date (hash-table)
  "Return an ISO-8601 date string for the latest date present in `chronometrist-events'."
  (--> (hash-table-keys hash-table)
       (sort it #'string-lessp)
       (last it)
       (first it)))

(defun ht-last (&optional (backend (chronometrist:active-backend)))
  "Return the last plist from `chronometrist-events'."
  (let* ((hash-table (chronometrist-backend-hash-table backend))
         (last-date  (ht-last-date hash-table)))
    (--> (gethash last-date hash-table)
         (last it)
         (car it))))

(defun ht-subset (start end hash-table)
  "Return a subset of HASH-TABLE.
The subset will contain values between dates START and END (both
inclusive).

START and END must be ts structs (see `local-time:timestamp'). They will be
treated as though their time is 00:00:00."
  (let ((subset (make-hash-table-1))
        (start  (midnight start))
        (end    (midnight end)))
    (maphash (lambda (key value)
               (when (ts-in start end (parse-timestring key))
                 (puthash key value subset)))
             hash-table)
    subset))
