(in-package :chronometrist)

(defun apply-time (time timestamp)
  "Return an instance of `local-time:timestamp' for TIMESTAMP, with time modified to TIME.

TIME must be integer seconds from midnight.

TIMESTAMP must be an instance of `local-time:timestamp'."
  (adjust-timestamp
      (adjust-timestamp timestamp
        (set :hour 0) (set :minute 0) (set :sec 0) (set :nsec 0))
    (offset :sec time)))

(defun parse-timestring (timestring)
  "Like `local-time:parse-timestring', but force
`local-time:*default-timezone*' to be `+utc-zone+'."
  (let ((*default-timezone* +utc-zone+))
    (local-time:parse-timestring timestring)))

(defun date-iso (&optional (ts (ts-now)))
  (ts-format "%F" ts))

(defun midnight (&optional (timestamp (now)))
  "Return a `local-time:timestamp' representing the midnight for TIMESTAMP.
TIMESTAMP should be an instance of `local-time:timestamp'."
  (adjust-timestamp timestamp
    (set :hour 0) (set :minute 0) (set :sec 0) (set :nsec 0)))

(defun format-time-iso8601 (&optional (timestamp (now)))
  "Return TIMESTAMP (or the current time) as an ISO-8601 date-time string.

TIMESTAMP should be an instance of `local-time:timestamp' acceptable
to `local-time:format-timestring'."
  (format-timestring nil timestamp
                     :format '((:year 4) "-" (:month 2) "-" (:day 2) "T"
                               (:hour 2) ":" (:min 2) ":" (:sec 2) :gmt-offset-or-z)))

(defun split-time (start-time stop-time day-start-time)
  "If START-TIME and STOP-TIME intersect DAY-START-TIME, split them into two intervals.

START-TIME and STOP-TIME must be instances of `local-time:timestamp'.

DAY-START-TIME must be a string in the form \"HH:MM:SS\" (see
`*day-start-time*')

Return a list in the form
\((:start START-TIME
  :stop <day-start time on initial day>)
 (:start <day start time on second day>
  :stop STOP-TIME))"
  ;; FIXME - time zones are ignored; may cause issues with
  ;; time-zone-spanning events

  ;; The time on which the first provided day starts (according to `*day-start-time*')
  (let* ((first-day-start (apply-time day-start-time
                                      start-time))
         (next-day-start  (adjust-timestamp first-day-start
                            (offset :hour 24))))
    ;; Does the event stop time exceed the next day start time?
    (when (timestamp< next-day-start stop-time)
      (list `(:start ,start-time :stop ,next-day-start)
            `(:start ,next-day-start :stop ,stop-time)))))

(defun seconds-to-hms (seconds)
  "Convert SECONDS to a vector in the form [HOURS MINUTES SECONDS].
SECONDS must be a positive integer."
  (let* ((seconds (truncate seconds))
         (s       (% seconds 60))
         (m       (% (/ seconds 60) 60))
         (h       (/ seconds 3600)))
    (list h m s)))

(defun format-duration-long (seconds)
  "Return SECONDS as a human-friendly duration string.
e.g. \"2 hours, 10 minutes\". SECONDS must be an integer. If
SECONDS is less than 60, return a blank string."
  (let* ((hours         (/ seconds 60 60))
         (minutes       (% (/ seconds 60) 60))
         (hour-string   (if (= 1 hours) "hour" "hours"))
         (minute-string (if (= 1 minutes) "minute" "minutes")))
    (cond ((and (zerop hours) (zerop minutes)) "")
          ((zerop hours)
           (format "%s %s" minutes minute-string))
          ((zerop minutes)
           (format "%s %s" hours hour-string))
          (t (format "%s %s, %s %s"
                     hours hour-string
                     minutes minute-string)))))

(defun iso-to-date (timestamp)
  (first (split-string timestamp :separator "T")))

(defun iso-to-julian-date (datestring)
  (astronomical-julian-date (parse-timestring datestring)))

(defun format-timestring (timestamp format)
  (local-time:format-timestring nil timestamp :format format))
