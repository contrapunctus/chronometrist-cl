(in-package :cl)
(defpackage :chronometrist
  (:use :cl :trivia)
  (:import-from :uiop
                #:xdg-config-home #:xdg-data-home
                #:strcat #:split-string)
  (:import-from :local-time
                #:now #:today #:timestamp-to-unix #:adjust-timestamp
                #:timestamp<
                #:*default-timezone* #:+utc-zone+
                #:unix-to-timestamp
                #:astronomical-julian-date
                #:timestamp #:timestamp-difference #:timestamp=)
  (:import-from :alexandria #:make-keyword)
  (:shadow #:remove)
  (:local-nicknames (:al :alexandria)
                    (:conf :ubiquitous)
                    (:s :serapeum)
                    (:r :rutils)
                    (:m :metabang-bind)
                    (:t :local-time)
                    (:tt :trivial-types)
                    (:nr :named-readtables))
  (:export #:readtable)

  ;; entries, intervals, and events
  (:export #:entry #:names #:properties
           #:interval #:start #:stop #:make-interval
           #:interval-equal?
           #:interval-duration
           #:split-interval
           #:trim-intervals
           #:intervals-split-p
           #:interval-unify
           #:interval-in-range?
           #:event)

  ;; customizable variables
  (:export #:global-value #:define-global-option
           #:task-option #:define-task-option
           #:load-configuration
           #:day-start-time #:week-start-day
           #:*active-backend*
           #:*sexp-pretty-print-function*)

  ;; configuration protocol
  (:export #:option #:global-option #:task-option
           #:global-value #:task-value)

  ;; predefined global options
  (:export #:database-directory
           #:database-pathname-name
           #:init-pathname
           #:allow-concurrent-intervals
           #:day-start-time
           #:week-start-day

           #:*database-directory*
           #:*database-pathname-name*
           #:*init-pathname*
           #:*allow-concurrent-intervals*
           #:*day-start-time*
           #:*week-start-day*)

  ;; predefined task options
  (:export #:show
           #:goal
           #:property-presets
           #:*tasks*)

  ;; backend protocol
  (:export #:backend #:*backends* #:active-backend #:define-backend
           #:backend-file
           #:task-list
           #:backend-empty-p #:backend-modified-p
           #:initialize #:cleanup
           #:intervals #:insert #:remove #:update
           #:on-change #:on-add #:on-modify #:on-remove
           #:migrate #:to-hash-table #:to-list
           #:list-intervals
           #:list-tasks
           #:active-days #:count-records
           #:file-backend-mixin #:elisp-sexp-backend
           #:extension)

  ;; extended protocol
  (:export #:latest-interval #:task-records-for-date #:latest-date-records
           #:current-task #:clock-in #:clock-out)

  ;; date-time and helpers
  (:export #:apply-time
           #:make-hash-table-1 #:iso-to-date #:plist-key-values
           #:task-duration
           #:midnight #:parse-timestring
           #:iso-to-julian-date
           #:format-timestring)

  ;; conditions
  (:export #:no-active-backend)

  ;; debugging
  (:export #:*debug-enable* #:*debug-buffer*))
