(in-package :chronometrist)

(defclass backend ()
  ((%task-list :initform nil
               :initarg :task-list
               :accessor backend-task-list))
  (:documentation "Protocol class for Chronometrist backends."))

(defvar *backends* nil
  "List of instances of `chronometrist:backend'.")

(defvar *active-backend* nil
  "The instance of `chronometrist:backend' currently in use.")

(define-condition no-active-backend (error) ()
  (:documentation
   "Condition raised when no active backend has been specified.")
  (:report
   (lambda (condition stream)
     (format stream "No active backend - `*active-backend*' is empty."))))

(defun active-backend ()
  "Return an object representing the currently active backend."
  (or *active-backend* (error 'no-active-backend)))

(defmacro define-backend (name direct-superclasses direct-slots &rest options)
  "Define an instance of `chronometrist:backend'.

Like `defclass', but also define a `*<name>*' special variable holding
an instance of the class.

If DIRECT-SUPERCLASSES do not contain `chronometrist:backend' or a
subclass of the same, `chronometrist:backend' is added to the
DIRECT-SUPERCLASSES."
  (let ((var-name (earmuff-symbol name)))
    `(progn
       (defclass ,name
           ,(if (loop for class in direct-superclasses
                        thereis (subtypep (find-class class)
                                          (find-class 'chronometrist:backend)))
                direct-superclasses
                (cons 'chronometrist:backend direct-superclasses))
         ,direct-slots ,@options)
       (defvar ,var-name (make-instance (quote ,name)))
       (setf *backends*
             (remove-if (lambda (backend)
                          (eq ',var-name (class-name (class-of backend))))
                        *backends*))
       (pushnew ,var-name *backends*)
       (unless *active-backend*
         (setf *active-backend* ,var-name))
       ,var-name)))

(defgeneric backend-empty-p (backend)
  (:documentation "Return non-nil if BACKEND contains no records, else nil."))

(defgeneric backend-modified-p (backend)
  (:documentation "Return non-nil if BACKEND is being modified.
For instance, a file-based backend could be undergoing editing by
a user."))

(defgeneric initialize (backend)
  (:documentation "Initialize and return BACKEND.

Disk-based backends should use this to create their file(s), if it
does not already exist."))

(defgeneric cleanup (backend)
  (:documentation "Perform cleanup for BACKEND. Called at application exit.")
  (:method (backend)))

(defgeneric intervals (backend &key start end)
  (:documentation
   "Return a list of inactive intervals from BACKEND, or NIL if no intervals were found.

Second return value is a list of active intervals.

Both returned lists should be in reverse chronological order.

If START and END are provided, return intervals intersecting with that
time range. START and END should be instances of
`local-time:timestamp'.

If END is NIL, return all intervals from START.

If BACKEND is file-based, it must signal a `FILE-DOES-NOT-EXIST'
condition if the file does not exist."))

(defgeneric insert (backend object &key &allow-other-keys)
  (:documentation "Insert OBJECT into BACKEND.

Return non-nil if insertion is successful.

OBJECT may be an instance of `chronometrist:interval' or
`chronometrist:event'."))

#+(or)
(defmethod insert :before ((_backend t) plist &key &allow-other-keys)
  (unless (typep plist 'plist)
    (error "Not a valid plist: %S" plist)))

(defgeneric update (backend old new)
  (:documentation "In BACKEND, update OLD object with NEW.

OBJECT may be an instance of `chronometrist:interval' or
`chronometrist:event'."))

(defgeneric remove (backend object &key &allow-other-keys)
  (:documentation "Remove OBJECT from BACKEND.

Return non-nil if OBJECT is successfully removed.

Signal an error if OBJECT was not found.

OBJECT may be an instance of `chronometrist:interval' or
`chronometrist:event'."))

#+(or)
(defmethod task-records-for-date :before
    ((_backend t) task date-ts &key &allow-other-keys)
  (unless (typep task 'string)
    (error "task %S is not a string" task))
  (unless (typep date-ts 'ts)
    (error "date-ts %S is not a `ts' struct" date-ts)))

(defgeneric on-add (backend)
  (:documentation "Function called when data is added to BACKEND.

This may happen within Chronometrist (e.g. via `insert') or outside it (e.g. a user editing the backend file).

NEW-DATA is the data that was added."))

(defgeneric on-modify (backend)
  (:documentation "Function called when data in BACKEND is modified (rather than added or removed).

This may happen within Chronometrist (e.g. via `update') or outside it (e.g. a user editing the backend file).

OLD-DATA and NEW-DATA is the data before and after the changes, respectively."))

(defgeneric on-remove (backend)
  (:documentation "Function called when data is removed from BACKEND.

This may happen within Chronometrist (e.g. via `remove-last') or outside it (e.g. a user editing the backend file).

OLD-DATA is the data that was modified."))

(defgeneric on-change (backend &rest args)
  (:documentation "Function to be run when BACKEND changes on disk.
This may happen within Chronometrist (e.g. via
`insert') or outside it (e.g. a user editing the
backend file)."))

(defgeneric verify (backend)
  (:documentation "Check BACKEND for errors in data.
Return nil if no errors are found.

If an error is found, return (LINE-NUMBER . COLUMN-NUMBER) for file-based backends."))

(defgeneric on-file-path-change (backend old-path new-path)
  (:documentation "Function run when the value of `file' is changed.
OLD-PATH and NEW-PATH are the old and new values of
`file', respectively."))

(defgeneric reset-backend (backend)
  (:documentation "Reset data structures for BACKEND."))

(defgeneric to-hash-table (backend)
  (:documentation
   "Return data in BACKEND as a hash table in chronological order.

Hash table keys are astronomical Julian days as integers.

Hash table values are lists of `interval' instances.

Both hash table keys and hash table values must be in chronological order."))

(defgeneric memory-layer-empty-p (backend)
  (:documentation "Return non-nil if memory layer of BACKEND contains no records, else nil."))

(named-readtables:in-readtable fn:fn-reader)

(defun task-duration
    (task &key (start (midnight))
            (end (t:adjust-timestamp start
                   (offset :hour 24)))
            (backend (active-backend))
            (intervals nil intervals-supplied?))
  "Return time spent on TASK between START and END.

TASK should be an instance of `chronometrist:task'.

The return value is a duration in seconds, as an integer."
  (let* ((intervals (if intervals-supplied?
                        (remove-if-not λ(interval-in-range? _ start end)
                                       intervals)
                        (multiple-value-bind (inactive active)
                            (intervals backend :start start :end end)
                          (append inactive active))))
         ;; filter intervals by task, then trim them to the given time range
         (intervals (s:~> (remove-if-not λ(chronometrist.task:interval-match? task _)
                                         intervals)
                          (trim-intervals _ start end))))
    ;; (format *debug-io* "~%task-duration:")
    ;; (format *debug-io* "~%  ~S" start)
    ;; (format *debug-io* "~%  ~S" end)
    ;; (format *debug-io* "~%  ~S" (chronometrist.task:name task))
    ;; (format *debug-io* "~%  ~S~%" intervals)
    (if intervals
        (reduce #'+ (mapcar #'interval-duration intervals))
        ;; no events for this task on DATE, i.e. no time spent
        0)))

(defvar *tasks* nil
  "List of tasks to be displayed by the Chronometrist frontend.
Value may be either nil or a list of strings.

If nil, the task list is generated from user data in
`*user-data-file*' and stored in the task-list slot of the
active backend.")

(defun active-time-on (&optional (date (midnight)))
  "Return the total active time today, or on DATE.
Return value is seconds as an integer."
  (->> (--map (task-duration it date) (*tasks*))
       (-reduce #'+)
       (truncate)))

(defun statistics-count-active-days (task table)
  "Return the number of days the user spent any time on TASK.
  TABLE must be a hash table - if not supplied, `chronometrist-events' is used.

  This will not return correct results if TABLE contains records
which span midnights."
  (loop :for events :being :the hash-values :of table
        :count (seq-find (lambda (event)
                           (equal task (getf event :name)))
                         events)))

(defun task-list ()
  "Return a list of tasks to be used for the frontend.
If `chronometrist:*tasks*' is non-nil, return its value;
otherwise, return a list of tasks from the active backend."
  (let ((backend (active-backend)))
    ;; (format *debug-io* "active backend: ~s~%" backend)
    (or *tasks*
        (backend-task-list backend)
        (setf (backend-task-list backend)
              (list-tasks backend)))))

(defclass file-backend-mixin ()
  ((%file :initform nil
          :initarg :file
          :reader file-name
          :type (or null pathname string)
          :documentation
          "Pathname for backend file, without extension. Do not access this
         directly - use `backend-file' instead.")
   (%extension :initform
               (error "File-based backends must specify an :EXTENSION.")
               :initarg :extension
               :accessor extension
               :type (or pathname string)
               :documentation
               "Extension of backend file.")
   (%hash-table :initform (make-hash-table-1)
                :initarg :hash-table
                :accessor backend-hash-table)
   (%file-watch :initform nil
                :initarg :file-watch
                :accessor backend-file-watch
                :documentation
                "Filesystem watch object, as returned by `file-notify-add-watch'."))
  (:documentation "Mixin for backends storing data in a single file."))

(defmethod backend-file ((backend file-backend-mixin))
  "Return a pathname for BACKEND.

If the BACKEND's `file-name' is non-nil, the pathname is based on the
same. Otherwise, the pathname is based on the configuration options
`database-pathname-name' and `database-directory'."
  (if (file-name backend)
      (make-pathname :type (extension backend)
                     :defaults (file-name backend))
      (make-pathname :type (extension backend)
                     :name (global-value *database-pathname-name*)
                     :defaults (global-value *database-directory*))))

(defgeneric migrate (input-backend output-backend
                     &key if-exists interval-function &allow-other-keys)
  (:documentation "Save data from INPUT-BACKEND to OUTPUT-BACKEND.

IF-EXISTS may be :ERROR (the default), :OVERWRITE, or :MERGE.

INTERVAL-FUNCTION should be a function that accepts and returns an
instance of `interval'. It is called for every `interval' to be
inserted.")
  (:method ((in backend) (out file-backend-mixin) &key (if-exists :error) interval-function)
    (m:bind ((file      (backend-file out))
             ((:values inactive active) (intervals in))
             (intervals (append (reverse inactive) (reverse active)))
             ((:flet call-interval-function (interval))
              (if interval-function
                  (funcall interval-function interval)
                  interval)))
      ;; (format *debug-io* "active ~A~%" active)
      (when (probe-file file)
        (case if-exists
          (:error (error 'file-exists :pathname file))
          (:overwrite
           (delete-file file)
           (cleanup out)
           (initialize out)
           ;; Insert while unifying split intervals
           (loop :with previous :with unified = 0 :with others = 0
                 :with length = (length intervals)
                 :for current :in intervals
                 :for i :from 1
                   :initially
                      (format *debug-io* "~%Importing ~A intervals~%" (length intervals))
                 :when previous :do
                   (cond ((intervals-split-p previous current)
                          (insert out
                                  (call-interval-function
                                   (interval-unify previous current)))
                          (incf unified)
                          ;; Ignore PREVIOUS and CURRENT - they have been inserted
                          (setf previous nil))
                         ;; Insert PREVIOUS, save CURRENT to compare with next
                         (t (insert out (call-interval-function previous))
                            (incf others)
                            (if (= i length)
                                (insert out (call-interval-function current))
                                (setf previous current))))
                 :else :do (setf previous current)
                           ;; :when (zerop (rem i 100)) :do
                           ;;   (format *debug-io* "~A " i)
                 :finally (format *debug-io*
                                  "~%Imported ~A intervals (~A unified, ~A unsplit)"
                                  (+ unified others) unified others)))
          (:merge (error ":MERGE not yet implemented")))))))
