(in-package :cl)
(defpackage :chronometrist.task
  (:use :cl)
  (:local-nicknames (:cm :chronometrist)
                    (:al :alexandria)
                    (:conf :ubiquitous)
                    (:s :serapeum)
                    (:r :rutils)
                    (:t :local-time)
                    (:tt :trivial-types)
                    (:nr :named-readtables))
  (:export #:readtable)
  (:export #:define-task #:*task-types*
           #:task #:name #:tags #:properties #:property-values
           #:parent #:children #:depth
           #:tags-match? #:properties-match? #:property-values-match? #:interval-match?
           #:task-interval #:task-equal?
           #:ancestors #:task-ancestor?
           #:map-task-tree #:task-tree-to-list)
  (:export #:simple-task
           #:make-simple-task)
  (:export #:inheriting-task
           #:make-inheriting-task)
  (:export #:property-task-mixin
           #:property-task
           #:make-property-task))
