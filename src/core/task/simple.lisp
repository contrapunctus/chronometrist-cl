(in-package :chronometrist.task)
(nr:in-readtable readtable)

(define-task simple-task (task update-parent-mixin) ()
  (:documentation
   "A task which displays and creates intervals based on a
   statically-defined set of tags, properties, and/or
   property-values."))

(defun make-simple-task (name &key (depth 0) parent (tags (list name)) properties children)
  (make-instance 'simple-task :name name :tags tags :properties properties
                              :depth depth :parent parent :children children))
