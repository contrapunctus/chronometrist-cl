(in-package :chronometrist.task)
(nr:in-readtable readtable)

(defclass property-task-mixin (simple-task) ()
  (:documentation
   "A task which displays intervals matching a certain property. The
   children of the task are the values of that property."))

(defun value-frequency-table (plists keyword)
  "Return a hash table whose hash keys are values used for KEYWORD in PLISTS, and whose hash values are the frequency of each value."
  (loop :with value-table = (make-hash-table :test #'equal)
        :for plist :in plists :do
          (loop :with current-kw
                :for elt :in plist
                :do (cond
                      ((keywordp elt)
                       (setf current-kw elt))
                      ((eq current-kw keyword)
                       (if (gethash elt value-table)
                           (incf (gethash elt value-table))
                           (setf (gethash elt value-table) 0)))))
        :finally (return value-table)))

(defun sort-table-keys (table)
  (sort (al:hash-table-keys table) #'> :key λ(gethash _ table)))

(defun property-children (task &optional (child-class 'task))
  "For a TASK having a single property, return a list of sub-tasks,
one for each existing value for the property."
  (let* ((property    (first (properties task :select)))
         (plists      (s:~> (cm:list-intervals (cm:active-backend))
                            (remove-if-not λ(interval-match? task _) _)
                            (mapcar #'cm:properties _)))
         (value-table (value-frequency-table plists property)))
    (loop :with depth = (1+ (depth task))
          :for value :in (sort-table-keys value-table)
          :collect
          #+(or)
            (pushnew (make-instance child-class
                                    :depth depth :parent task :tags nil
                                    :property-values (list property value))
                     (slot-value task '%children)
                     :test #'task-equal?)
            (make-instance child-class
                           :name (format nil "~A" value)
                           :depth depth
                           :parent task
                           :tags (slot-value task '%tags)
                           :property-values (list property value)))))

(defmethod children ((task property-task-mixin))
  (property-children task))

(defclass inheritable-property-task (property-task-mixin inheritable-task) ()
  (:documentation
   "A task which displays intervals matching certain tags (inherited
   from its ancestors) and certain properties. The children of the
   task are the values of that property."))

(defclass property-task (property-task-mixin) ()
  (:documentation
   "A task which displays intervals matching certain properties, and
   whose children are the values of that property."))

(defun make-property-task (property &key name (depth 0) parent tags)
  (let ((name (or name (string-downcase (symbol-name property)))))
    (make-instance 'property-task
                   :name name :tags tags :properties (list property)
                   :depth depth :parent parent)))

(defmethod tags ((task inheriting-task) (operation (eql :select)))
  "Return a list of tag sets, where each set is a list of strings
corresponding to the tags of each descendant of TASK."
  #+(or) (map-task-tree #'name (list task))
  (map-task-tree λ(mapcar #'name (ancestors _)) (list task)))

(defmethod tags ((task inheriting-task) (operation (eql :insert)))
  (mapcar #'name (ancestors task)))

(defmethod properties ((task inheriting-task) (operation (eql :select)))
  )

(defmethod properties ((task inheriting-task) (operation (eql :insert)))
  )

(defun %children-bound-and-true? (task)
  (and (slot-boundp task '%children)
       (slot-value task '%children)))

(defun keyword-frequency-table (plists)
  (loop :with keyword-table = (make-hash-table)
        :for plist :in plists :do
          (loop :with keyword
                :for elt :in plist
                :if (keywordp elt) :do
                  (if (gethash elt keyword-table)
                      (incf (gethash elt keyword-table))
                      (setf (gethash elt keyword-table) 0))
                  (setf keyword elt))
        :finally (return keyword-table)))

;; For a given task, return a list of child tasks, one for each unique
;; property used for the task. Each property subtask has one child
;; task for each unique value used with the property.
(defun task-property-subtree (task &rest keywords)
  "Return a list of `task' instances, each being a property used in
TASK's `properties'. The children of each property sub-task are the
values for that property."
  (r:bind ((inactive active (cm:intervals (cm:active-backend)))
           (intervals (remove-if-not λ(interval-match? task _)
                                     (append active inactive)))
           (plists    (s:~> (mapcar #'cm:properties intervals)
                            (remove-if-not #'identity _)
                            (remove-duplicates _ :test #'equal)))
           (keyword-table (keyword-frequency-table plists)))
    ;; (format *debug-io* "intervals: ~S~%" intervals)
    (loop
      :with depth = (1+ (depth task))
      :for keyword :in (sort-table-keys keyword-table)
      :for name = (string-capitalize (symbol-name keyword))
      :for value-table = (value-frequency-table plists keyword)
      :for property-task = (make-simple-task name :depth depth
                                           :parent task
                                           :tags nil
                                           :children children)
      :for children = (loop
                        :with depth = (1+ depth)
                        :for value :in (sort-table-keys value-table)
                        :for name = (format nil "~A" value)
                        :collect
                        (make-simple-task name :depth depth
                                        :parent property-task
                                        :tags nil
                                        :properties (list keyword value)))
      :collect property-task)))
