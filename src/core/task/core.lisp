(in-package :chronometrist.task)
(nr:in-readtable readtable)

(defvar *task-types* nil "Available task types as a list of symbols.")

(defmacro define-task (name direct-superclasses direct-slots &rest options)
  "Define a user-facing task class called NAME.

Like `defclass', but NAME is added to `*task-types*'.

DIRECT-SUPERCLASSES should include `chronometrist.task:task' or one of its subclasses."
  `(progn
     (defclass ,name ,direct-superclasses ,direct-slots ,@options)
     (pushnew ',name *task-types*)))

;; What's a term that can describe both the activity undertaken during
;; a time range, and the name of an event or incident? Don't say
;; "name" :\
;; Choosing "task" for now - existing code and docs already use it,
;; and events haven't really been implemented yet.
(defclass task ()
  ((%name :initarg :name :accessor name :initform (error "Task name must be specified")
          :documentation "Name to be displayed for this task.")
   (%tags :initarg :tags
          :documentation
          "A list of strings matched against interval names, and inserted in the names of new intervals created for this task.")
   (%properties
    :initarg :properties :initform nil
    :documentation
    "List of keywords. `properties' of matching `interval' instances must contain each of these keywords.")
   (%property-values
    :initarg :property-values :initform nil
    :documentation
    "Plist to be matched against interval `properties', and inserted in the properties of new intervals created for this task.")
   (%parent :initarg :parent :accessor parent)
   (%children :initarg :children :accessor children :initform nil)
   (%depth :initarg :depth :initform 0 :accessor depth))
  (:documentation
   "Protocol class representing a 'task' in a Chronometrist interface."))

(defclass update-parent-mixin () ()
  (:documentation
   "Mixin which automatically adds a task to the `children' slot of its `parent'."))

(defmethod initialize-instance :after
    ((task update-parent-mixin) &rest initargs &key &allow-other-keys)
  ;; Add TASK to CHILDREN of PARENT
  (when (parent task)
    (let ((children (reverse (children (parent task)))))
      (pushnew task children :test #'task-equal?)
      (setf (children (parent task)) (reverse children)))))

(defmethod print-object ((task task) stream)
  (print-unreadable-object (task stream :type t)
    (let ((parent   (parent task))
          (children (children task)))
      (format stream "~S ~S ~S ~S ~S"
              (name task)
              (tags task :select)
              (depth task)
              (and parent (name parent))
              (and children (mapcar #'name children))))))

(defgeneric tags (task operation)
  (:documentation
   "Return a list of strings for TASK and OPERATION.

OPERATION can be :SELECT, which means the tags are intended to be
matched against existing intervals, or :INSERT, which means the tags
are intended to be inserted into the `names' of a new instance of
`interval'.")
  (:method ((task task) operation)
    (when (slot-boundp task '%tags)
      (slot-value task '%tags))))

(defgeneric properties (task operation)
  (:documentation
   "Return a list of keywords for TASK and OPERATION.

OPERATION can be :SELECT, which means the keywords are intended to be
matched against the `properties' of existing intervals, or :INSERT,
which means the properties are intended to be inserted into the
`properties' of a new instance of `interval'.")
  (:method ((task task) operation)
    (slot-value task '%properties)))

(defgeneric property-values (task operation)
  (:documentation
   "Return a property list for TASK and OPERATION.

OPERATION can be :SELECT, which means the plist is intended to be
matched against the `properties' of existing intervals, or :INSERT,
which means the plist is intended to be inserted into the `properties'
of a new instance of `interval'.")
  (:method ((task task) operation)
    (slot-value task '%property-values)))

(defgeneric tags-match? (task interval)
  (:documentation "Return non-nil if INTERVAL has the same tags as TASK.")
  (:method ((task task) (interval cm:interval))
    (let ((interval-tags (s:~> (cm:names interval)
                               (copy-seq _)
                               (sort _ #'string-lessp)))
          (task-tags     (tags task :select)))
      (if task-tags
          (every λ(member _ interval-tags :test #'equal) task-tags)
          t))))

(defgeneric properties-match? (task interval)
  (:documentation "Return non-nil if INTERVAL has the same properties as TASK.")
  (:method ((task task) (interval cm:interval))
    (let ((interval-properties (cm:properties interval))
          (task-properties     (properties task :select)))
      (cond ((not task-properties) t)
            ((every #'keywordp task-properties)
             (remove-if-not λ(getf interval-properties _) task-properties))))))

(defgeneric property-values-match? (task interval)
  (:documentation "Return non-nil if INTERVAL has the same property-values as TASK.")
  (:method ((task task) (interval cm:interval))
    (let ((interval-properties (cm:properties interval))
          (task-plist          (property-values task :select)))
      (cond ((not task-plist) t)
            ((tt:property-list-p task-plist)
             (loop :for (keyword value) :on task-plist :by #'cddr
                   :when (equal value (getf interval-properties keyword))
                     :collect it))))))

(defgeneric interval-match? (task interval)
  (:documentation "Return non-nil if INTERVAL should be considered part of TASK.")
  (:method ((task task) (interval cm:interval))
    (and (tags-match? task interval)
         (properties-match? task interval)
         (property-values-match? task interval))))

(defgeneric task-interval (task)
  (:documentation "Return an INTERVAL for TASK.")
  (:method ((task task))
    (cm:make-interval (tags task :insert) (t:now) nil
                      (property-values task :insert))))

(defun task-equal? (a b)
  (and (typep a 'task)
       (typep b 'task)
       (equal (name a)
              (name b))
       (eq (depth a)
           (depth b))
       (eq (length (children a))
           (length (children b)))
       (every #'task-equal? (children a) (children b))))

(defun ancestors (task)
  "Return a list containing TASK and each of its ancestors."
  (loop :with acc = (list task)
        :with i = task
        :while (parent i)
        :do (push (parent i) acc)
            (setf i (parent i))
        :finally
           (return
             (sort acc (lambda (a b)
                         (string-lessp (name a) (name b)))))))

(defun map-task-tree (function task-tree)
  "Call FUNCTION with each descendent node of TASK-TREE.
TASK-TREE should be a list of `task' instances."
  (let ((acc '()))
    (labels ((helper (function task-tree)
               (al:when-let ((task (first task-tree)))
                 (push (funcall function task) acc)
                 (when (children task)
                   (helper function (children task)))
                 (helper function (rest task-tree)))))
      (helper function task-tree)
      (reverse acc))))

(defun task-tree-to-list (task-tree)
  (map-task-tree #'identity task-tree))

(defun task-ancestor? (ancestor successor)
  "Return non-nil if task A is an ancestor of task B."
  (member ancestor (ancestors successor) :test #'task-equal?))
