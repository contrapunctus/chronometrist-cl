(in-package :chronometrist.task)
(nr:in-readtable readtable)

(define-task inheriting-task (simple-task) ()
  (:documentation
   "A task whose tags are derived from its own name and the names of
   its ancestors (for new intervals)/successors (for display)."))

(defun make-inheriting-task (name &key (depth 0) parent properties children)
  (make-instance 'inheriting-task
                 :name name :properties properties
                 :depth depth :parent parent :children children))

;; XXX: this means that even parent tasks which are not of type
;; `inheriting-task' will have their tags ignored and their `name'
;; used as their `tags'...
(defmethod tags ((task inheriting-task) (operation (eql :select)))
  "Return a list of tag sets, where each set is a list of strings corresponding to the tags of each descendant of TASK."
  #+(or)
  (map-task-tree #'name (list task))
  (map-task-tree λ(mapcar #'name (ancestors _)) (list task)))

(defmethod tags ((task inheriting-task) (operation (eql :insert)))
  (mapcar #'name (ancestors task)))

(defmethod properties ((task inheriting-task) (operation (eql :select)))
  )

(defmethod properties ((task inheriting-task) (operation (eql :insert)))
  )

(defmethod tags-match? ((task inheriting-task) (interval cm:interval))
  (let ((interval-tags (sort (copy-seq (cm:names interval)) #'string-lessp))
        (task-tags     (tags task :select)))
    (member interval-tags task-tags :test #'equal)))
