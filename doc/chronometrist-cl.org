# Created 2023-03-03 Fri 19:42
#+options: prop:t toc:3
#+title: Chronometrist
#+author: contrapunctus
#+subtitle: Friendly and powerful personal time tracker/analyzer in CLIM
#+export_file_name: chronometrist-cl.org
#+description: User Manual

* Explanation
:PROPERTIES:
:DESCRIPTION: The design, the implementation, and a little history
:CUSTOM_ID: explanation
:END:
Chronometrist is a friendly and powerful personal time tracker and analyzer. This is a Common Lisp port of the [[https://codeberg.org/contrapunctus/chronometrist][Emacs Lisp original]].

Currently, it contains
1. an import-only plist-group backend
2. an SQLite backend (nearly complete)
3. a [[https://github.com/robert-strandh/Clobber][Clobber]] (object store) backend (nearly complete)
4. a [[https://mcclim.common-lisp.dev/][CLIM]] frontend (WIP)

Currently, this port can -
1. import from a plist-group file and export to an SQLite or Clobber database
   #+begin_src lisp
      (ql:quickload '(chronometrist-plist chronometrist-clobber))
      (in-package :chronometrist)
      (let ((plg (make-instance 'chronometrist.plist-group:backend
                                :file #P"/path/to/old/file"))
            (clobber (make-instance 'chronometrist.clobber:backend
                                    :file #P"/path/to/new/file")))
        (chronometrist:initialize clobber)
        (chronometrist:migrate plg clobber :if-exists :overwrite))
   #+end_src

2. display the CLIM GUI -
   #+begin_src lisp
      (ql:quickload '(chronometrist-clim))
      (chronometrist-clim:run)
   #+end_src

See [[file:TODO.org][TODO.org]] for ongoing work, future plans, and ideas.

** Characteristics
:PROPERTIES:
:CUSTOM_ID: explanation-characteristics
:END:
1. Made for personal use. By default, your data is stored on your machine and is only accessible to you.
2. Don't make assumptions about the user's profession.
3. (WIP) Hooks to integrate time tracking into your workflow.
4. (WIP) Make it easy to edit and correct data.
5. (WIP) Support mouse and keyboard use equally.
6. (Planned) Extensions to automatically insert data from a variety of sources.

** Motivation for the Common Lisp port
:PROPERTIES:
:CUSTOM_ID: explanation-common-lisp-port
:END:
In March 2022, work began on the long-awaited Common Lisp port of Chronometrist, which aims to create -
1. a greater variety of backends (e.g. SQLite)
2. a common reusable library for frontends to use,
3. a greater variety of frontends, such as -
   - a command line interface (CLI), for UNIX scripting;
   - a terminal user inteface (TUI), for those so inclined;
   - a CLIM (Common Lisp Interface Manager) GUI [fn:1],
   - Qt and Android interfaces using [[https://gitlab.com/eql/lqml][LQML]],
   - web frontends (possibly via [[https://common-lisp.net/project/parenscript/][Parenscript]] or [[https://github.com/rabbibotton/clog][CLOG]]),
   - and perhaps even an interface for wearable devices!

The port was also driven by the desire to have access to Common Lisp's better performance, and features such as namespaces, a /de facto/ standard build system, multithreading, SQLite bindings, a more fully-featured implementation of CLOS and MOP, and type annotations, checking, and inference.

The older Emacs Lisp codebase will probably become an Emacs frontend to a future Common Lisp CLI client.

[fn:1] McCLIM also has an incomplete ncurses backend - when completed, a CLIM frontend could provide a TUI "for free".

** Source code overview
:PROPERTIES:
:CUSTOM_ID: source-code-overview
:END:
*** Data structures
:PROPERTIES:
:CREATED:  2022-07-20T19:56:55+0530
:CUSTOM_ID: explanation-data-structures
:END:
**** Intervals
:PROPERTIES:
:CREATED:  2022-11-05T18:49:21+0530
:CUSTOM_ID: intervals
:END:
Chronometrist stores ranges of time as instances of =interval=. Each =interval= has a =names= (a list of strings), =start= and =stop= timestamps, and optional =properties= (a Lisp property list).

Intervals which represent ongoing activities are said to be "active", and have no =stop= time associated with them.

It is planned to add support for storing events, as instances of =event=. An =event=, much like an =interval=, has a =name= and optional =properties=, but it is only associated with a =time= (timestamp) rather than a time range.

As far as possible, instances of =local-time:timestamp= are used to represent time, on account of the variety of operations available for them. [[file:src/core/time.lisp][time.lisp]] defines various time-related helpers.

**** Tasks
:PROPERTIES:
:CREATED:  2022-11-05T18:49:26+0530
:CUSTOM_ID: tasks
:END:
The concept of a task serves two purposes -
- Output :: A time tracker may display a flat list of tasks. Some users may prefer a foldable tree instead. The contents and hierarchy of these tasks may differ between users and situations.
- Input :: A task can be clocked into, i.e. an interval is inserted into the backend with certain tags and/or property-values.

To support this, Chronometrist provides a tree of =task= instances. A "task" is essentially a query, a means of selecting intervals by various means -
1. the names present in the intervals
2. the property-values in the interval property lists
3. whether the intervals pass a given filter function

Each =task= contains a =name= (a string), =depth= (as an integer), an optional =parent= (an instance of =task=), and optional =children= (a list of instances of =task=).

While the task hierarchy affects how data is displayed and entered, neither the hierarchy nor instances of =task= are actually stored in instances of =interval=. This allows the user to change the task hierarchy while minimizing its effect on the data displayed in the frontend.

For instance, if the user has a task tree ~Foo → Bar → Quux~ and inheritance is enabled, clocking into ~Quux~ will create an interval whose =names= are =("Bar" "Foo" "Quux")= (sorted alphabetically). Later, if the user changes their task tree to ~Quux → Bar → Foo~, clocking into ~Foo~ will still create an interval whose =names= are =("Bar" "Foo" "Quux")=. Intervals and durations which appeared under ~Quux~ before will now show up under ~Foo~, because they both have the same ancestors and are equivalent in our view.

***** The basic =task= class
:PROPERTIES:
:CREATED:  2022-11-13T10:28:35+0530
:CUSTOM_ID: basic-task-class
:END:
With the basic =task= class, the user specifies tags, properties, and/or property-values, which are used to determine both what intervals are displayed, as well as what tags, properties, and/or property-values are inserted into new intervals.

***** The =inheriting-task= class
:PROPERTIES:
:CREATED:  2022-11-13T10:29:32+0530
:CUSTOM_ID: inheriting-task-class
:END:
This is designed to create tasks which display the intervals matched by their children, and which may (configurably) insert the tags of their ancestors when creating new intervals.

The user is expected to only specify task names and children. Tags are derived from names.

*** Configuration
:PROPERTIES:
:CREATED:  2022-08-01T20:30:16+0530
:CUSTOM_ID: explanation-configuration
:END:
=chronometrist-cl='s configuration has the following requirements -
1. Users should be able to make persistent changes to the configuration, without requiring Lisp knowledge or changes to the init.lisp
2. Extensions should be able to add new configuration options.

Configuration is implemented using the [[https://github.com/Shinmera/ubiquitous][Ubiquitous]] library.

=chronometrist-cl= has two kinds of configuration options -
1. global, which have a single value applicable to the entire program, and
2. task-specific, which can potentially have a different value for each task.

Ubiquitous stores the configuration as a (serialization of a) hash table with the keys =:global= and =:task=, corresponding to the aforementioned types of configuration.

The =:task= key has another hash table as a value, whose keys are all tasks (as strings), with the exception of a single =:default= key. The value of the =:default= key is a hash table containing default values for task-specific options.

Note that while =define-global-option= and =define-task-option= require a non-keyword symbol as the option name (mostly for consistency with =defun= etc), options are defined as keywords based on the name, and thus all other functions expect options to be keywords.

See also - [[#reference-configuration][reference documentation for the configuration protocol]].

*** CLIM frontend
:PROPERTIES:
:CUSTOM_ID: explanation-clim-frontend
:END:
The CLIM frontend uses CLIM panes for each view of data Chronometrist provides. Each pane has a [[file:chronometrist.org::#display-pane][=display-pane=]] method to display its contents.

Several different CLIM panes implementing different kinds of views have been experimented with, and these are present in the source code, even if not used. Work is underway to make a single "unified" pane, with the aim of presenting a single information-rich view which [[https://web.archive.org/web/20221020163351/http://worrydream.com/MagicInk/][minimizes user interaction]].

**** Tables
:PROPERTIES:
:CUSTOM_ID: explanation-clim-frontend-tables
:END:
The tables in the CLIM frontend are designed to be easy to extend.

The =table= class represents a table. The columns displayed in the table are accessed by the =table-schema (table)= method, which returns a list of =column= instances.

The =row= class provides row-specific information, such as the index of the row. (Provided by default through the =index-mixin= class and =index= accessor.)

The =column= class represents a column descriptor (a list of which forms a =table-schema=), and may be subclassed to provide any column-specific information, such as the label text for the column. (Provided by default through the =label= accessor.) Pre-defined =column= subclasses include =index-column= and =task-column=.

The =display-cell= method -
1. determines the contents of a cell based on the row, column, table, frame, and pane,
2. displays the contents (possibly delegating to a CLIM =present= method), and
3. returns the contents of the cell.
   - This is necessary to be able to mutably hold state as well as aid testing.

** Midnight-spanning intervals
:PROPERTIES:
:DESCRIPTION: Events starting on one day and ending on another
:CUSTOM_ID: explanation-midnight-spanning-intervals
:END:
A unique problem in working with Chronometrist, one I had never foreseen, was intervals which start on one day and end on another. For instance, you start working on something at =2021-01-01 23:00= hours and stop on =2021-01-02 01:00=.

If not accounted for, midnight-spanning intervals can mess up data consumption in all sorts of unforeseen ways. Consider two of the most common operations throughout the program -

1. Attempting to find the intervals belonging to a given date might end up with missing midnight-spanning intervals, near the start and the end of the day;

2. Attempting to find the time spent on a task on a given day can yield inaccurate results. If the day's intervals used for this contain a midnight-spanning interval, the duration will include the previous day's time from the interval as well as the target day's.

There are a few different approaches for dealing with them. In this implementation, all backend reader functions can potentially return midnight-spanning intervals, and it's up to the client code to split them if necessary.

See [[#reference-interval][interval operations]] for functions to split midnight-spanning intervals, check whether two intervals were split from one, and unify two split intervals into one.

** License
:PROPERTIES:
:CUSTOM_ID: explanation-license
:END:
I'd /like/ for all software to be liberated - transparent, trustable, and accessible for anyone to use, study, or improve.

I'd /like/ anyone using my software to credit me for the work.

I'd /like/ to receive financial support for my efforts, so I can spend all my time doing what I find meaningful.

But I don't want to make demands or threats (e.g. via legal conditions) to accomplish all that, nor restrict my services to only those who can pay.

Thus, Chronometrist is released under your choice of [[https://unlicense.org/][Unlicense]] or the [[http://www.wtfpl.net/][WTFPL]].

(See files [[file:UNLICENSE][UNLICENSE]] and [[file:WTFPL][WTFPL]]).

** Thanks
:PROPERTIES:
:CUSTOM_ID: explanation-thanks
:END:
beach for trusting a passionate-but-inexperienced newbie with funding.

jackdaniel and other contributors for laying the foundations for this application through their work on McCLIM, and answering my regular stream of questions.

gilberth, hayley, ck_, rotateq, and many others for helping me out with CLIM and Common Lisp.

The task duration pane was inspired by the Android application, [[https://github.com/netmackan/ATimeTracker][A Time Tracker]]

* Reference
:PROPERTIES:
:CREATED:  2022-07-20T21:28:12+0530
:CUSTOM_ID: reference
:END:

** Entries
:PROPERTIES:
:CREATED:  2022-07-20T23:51:24+0530
:CUSTOM_ID: reference-entries
:END:
#+results: 
**** =names (object)=                                      :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-names
:END:
**** =entry (standard-object)=                                        :class:
:PROPERTIES:
:CUSTOM_ID: reference-entry
:END:
Protocol class for entries in the Chronometrist database.

**** =interval (chronometrist:entry)=                                 :class:
:PROPERTIES:
:CUSTOM_ID: reference-interval
:END:
A time range spent on a specific task, with optional properties.

**** =make-interval (names start &optional stop properties)=       :function:
:PROPERTIES:
:CUSTOM_ID: reference-make-interval
:END:
**** =interval-equal? (a b)=                                       :function:
:PROPERTIES:
:CUSTOM_ID: reference-interval-equal?
:END:
**** =interval-duration (interval)=                                :function:
:PROPERTIES:
:CUSTOM_ID: reference-interval-duration
:END:
**** =split-interval (interval &optional (day-start-time (global-value *day-start-time*)))= :function:
:PROPERTIES:
:CUSTOM_ID: reference-split-interval
:END:
Return a list of two intervals if INTERVAL spans a midnight, else nil.

**** =trim-intervals (intervals start end)=                        :function:
:PROPERTIES:
:CUSTOM_ID: reference-trim-intervals
:END:
Return INTERVALS with timestamps changed to fall between START and END.

**** =intervals-split-p (old new)=                                 :function:
:PROPERTIES:
:CUSTOM_ID: reference-intervals-split-p
:END:
Return t if intervals OLD and NEW are split.

Split intervals means that the =stop= time of OLD must be the same as the =start= time of NEW, and they must have identical properties (except =start= and =stop=).

**** =interval-unify (old new)=                                    :function:
:PROPERTIES:
:CUSTOM_ID: reference-interval-unify
:END:
Return a single interval which has the =start= time of OLD and the =stop= time of NEW.

OLD and NEW must be instances of =chronometrist:interval=.

**** =event (chronometrist:entry)=                                    :class:
:PROPERTIES:
:CUSTOM_ID: reference-event
:END:
A named timestamp with optional properties.


** Tasks
:PROPERTIES:
:CREATED:  2022-11-14T10:46:06+0530
:CUSTOM_ID: tasks-1
:END:
#+results: 
**** =*task-types*=                                                :variable:
:PROPERTIES:
:CUSTOM_ID: reference-*task-types*
:END:
Available task types as a list of symbols.

**** =define-task (name direct-superclasses direct-slots &rest options)= :macro:function:
:PROPERTIES:
:CUSTOM_ID: reference-define-task
:END:
Define a user-facing task class called NAME.

Like =defclass=, but NAME is added to =*task-types*=.

DIRECT-SUPERCLASSES should include =chronometrist.task:task= or one of its subclasses.

**** =task (chronometrist-clim:task-mixin standard-object)=           :class:
:PROPERTIES:
:CUSTOM_ID: reference-task
:END:
Protocol class representing a 'task' in a Chronometrist interface.

**** =name (object)=                                       :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-name
:END:
**** =tags (task operation)=                               :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-tags
:END:
Return a list of strings for TASK and OPERATION.

OPERATION can be :SELECT, which means the tags are intended to be matched against existing intervals, or :INSERT, which means the tags are intended to be inserted into the =names= of a new instance of =interval=.

**** =properties (task operation)=                         :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-properties
:END:
Return a list of keywords for TASK and OPERATION.

OPERATION can be :SELECT, which means the keywords are intended to be matched against the =properties= of existing intervals, or :INSERT, which means the properties are intended to be inserted into the =properties= of a new instance of =interval=.

**** =property-values (task operation)=                    :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-property-values
:END:
Return a property list for TASK and OPERATION.

OPERATION can be :SELECT, which means the plist is intended to be matched against the =properties= of existing intervals, or :INSERT, which means the plist is intended to be inserted into the =properties= of a new instance of =interval=.

**** =parent (object)=                                     :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-parent
:END:
**** =children (object)=                                   :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-children
:END:
**** =depth (object)=                                      :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-depth
:END:
**** =tags-match? (task interval)=                         :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-tags-match?
:END:
Return non-nil if INTERVAL has the same tags as TASK.

**** =properties-match? (task interval)=                   :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-properties-match?
:END:
Return non-nil if INTERVAL has the same properties as TASK.

**** =property-values-match? (task interval)=              :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-property-values-match?
:END:
Return non-nil if INTERVAL has the same property-values as TASK.

**** =interval-match? (task interval)=                     :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-interval-match?
:END:
Return non-nil if INTERVAL should be considered part of TASK.

**** =task-interval (task)=                                :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-task-interval
:END:
Return an INTERVAL for TASK.

**** =task-equal? (a b)=                                           :function:
:PROPERTIES:
:CUSTOM_ID: reference-task-equal?
:END:
**** =ancestors (task)=                                            :function:
:PROPERTIES:
:CUSTOM_ID: reference-ancestors
:END:
Return a list containing TASK and each of its ancestors.

**** =task-ancestor? (ancestor successor)=                         :function:
:PROPERTIES:
:CUSTOM_ID: reference-task-ancestor?
:END:
Return non-nil if task A is an ancestor of task B.

**** =map-task-tree #'task-tree=                                   :function:
:PROPERTIES:
:CUSTOM_ID: reference-map-task-tree
:END:
Call FUNCTION with each descendent node of TASK-TREE. TASK-TREE should be a list of =task= instances.

**** =task-tree-to-list (task-tree)=                               :function:
:PROPERTIES:
:CUSTOM_ID: reference-task-tree-to-list
:END:

*** Simple task
:PROPERTIES:
:CREATED:  2022-11-15T18:36:47+0530
:CUSTOM_ID: simple-task
:END:
#+results: 
**** =simple-task (chronometrist.task:task chronometrist.task::update-parent-mixin)= :class:
:PROPERTIES:
:CUSTOM_ID: reference-simple-task
:END:
A task which displays and creates intervals based on a    statically-defined set of tags, properties, and/or    property-values.

**** =make-simple-task (name &key (depth 0) parent (tags (list name)) properties children)= :function:
:PROPERTIES:
:CUSTOM_ID: reference-make-simple-task
:END:

*** Inheriting task
:PROPERTIES:
:CREATED:  2022-11-15T18:36:53+0530
:CUSTOM_ID: inheriting-task
:END:
#+results: 
**** =inheriting-task (chronometrist.task:simple-task)=               :class:
:PROPERTIES:
:CUSTOM_ID: reference-inheriting-task
:END:
A task whose tags are derived from its own name and the names of    its ancestors (for new intervals)/successors (for display).

**** =make-inheriting-task (name &key (depth 0) parent properties children)= :function:
:PROPERTIES:
:CUSTOM_ID: reference-make-inheriting-task
:END:

** Time operations
:PROPERTIES:
:CREATED:  2022-07-20T23:52:05+0530
:CUSTOM_ID: reference-time
:END:
#+results: 
**** =apply-time (time timestamp)=                                 :function:
:PROPERTIES:
:CUSTOM_ID: reference-apply-time
:END:
Return an instance of =local-time:timestamp= for TIMESTAMP, with time modified to TIME.

TIME must be integer seconds from midnight.

TIMESTAMP must be an instance of =local-time:timestamp=.

**** =midnight (&optional (timestamp (now)))=                      :function:
:PROPERTIES:
:CUSTOM_ID: reference-midnight
:END:
Return a =local-time:timestamp= representing the midnight for TIMESTAMP. TIMESTAMP should be an instance of =local-time:timestamp=.


** Configuration protocol
:PROPERTIES:
:CREATED:  2022-07-31T23:13:53+0530
:CUSTOM_ID: reference-configuration
:END:
#+results: 
**** =option (standard-object)=                                       :class:
:PROPERTIES:
:CUSTOM_ID: reference-option
:END:
Protocol class for all configuration options.

**** =global-option (chronometrist:option)=                           :class:
:PROPERTIES:
:CUSTOM_ID: reference-global-option
:END:
Protocol class for options which have one value for the entire application.

**** =task-option (chronometrist:option)=                             :class:
:PROPERTIES:
:CUSTOM_ID: reference-task-option
:END:
Protocol class for options which can have different values for each task.

**** =global-value (option)=                               :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-global-value
:END:
Return the configuration value for OPTION. OPTION must be a keyword.

**** =global-value (new-value option)=                     :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-global-value
:END:
Set the value for configuration OPTION to NEW-VALUE. OPTION must be a keyword.

**** =task-value (task option)=                            :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-task-value
:END:
Return the TASK-specific configuration value for OPTION.

TASK must be a string. OPTION must be a keyword.

If a TASK-specific value is not found, return a default value.

The secondary value is T if the task-specific value was provided, and NIL if the default was used.

**** =task-value (new-value task keyword)=                 :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-task-value
:END:
Set the TASK-specific configuration value for OPTION to NEW-VALUE.

TASK must be a string. OPTION must be a keyword.


#+results: 
**** =define-global-option (name default &key documentation (value-class t))= :macro:function:
:PROPERTIES:
:CUSTOM_ID: reference-define-global-option
:END:
Define an application-wide configuration option called NAME.

The values for this option must be instances of CLASS.

DEFAULT is the default value for this option.

**** =define-task-option (name default &key documentation (value-class t))= :macro:function:
:PROPERTIES:
:CUSTOM_ID: reference-define-task-option
:END:
Define a task-specific configuration option called NAME.

The values for this option must be instances of VALUE-CLASS.

DEFAULT is the default value for this option.


#+results: 
**** =load-configuration ()=                                       :function:
:PROPERTIES:
:CUSTOM_ID: reference-load-configuration
:END:

*** Predefined options
:PROPERTIES:
:CREATED:  2022-08-03T21:09:53+0530
:CUSTOM_ID: predefined-options
:END:
**** Global options
:PROPERTIES:
:CREATED:  2022-08-03T21:11:33+0530
:CUSTOM_ID: global-options
:END:
#+results: 
***** =database-directory (chronometrist:global-option)=              :class:
:PROPERTIES:
:CUSTOM_ID: reference-database-directory
:END:
Absolute pathname for the database directory.

***** =database-pathname-name (chronometrist:global-option)=          :class:
:PROPERTIES:
:CUSTOM_ID: reference-database-pathname-name
:END:
Base name for the database file.

***** =init-pathname (chronometrist:global-option)=                   :class:
:PROPERTIES:
:CUSTOM_ID: reference-init-pathname
:END:
Absolute pathname for the user's init file.

***** =allow-concurrent-intervals (chronometrist:global-option)=      :class:
:PROPERTIES:
:CUSTOM_ID: reference-allow-concurrent-intervals
:END:
Whether to allow the creation of concurrent intervals.

***** =day-start-time (chronometrist:global-option)=                  :class:
:PROPERTIES:
:CUSTOM_ID: reference-day-start-time
:END:
Time at which a day is considered to start, as number of seconds from midnight.

***** =week-start-day (chronometrist:global-option)=                  :class:
:PROPERTIES:
:CUSTOM_ID: reference-week-start-day
:END:
Day on which the week is considered to start, as an integer between 0-6.


**** Task options
:PROPERTIES:
:CREATED:  2022-08-03T21:11:35+0530
:CUSTOM_ID: task-options
:END:
#+results: 
***** =show (chronometrist:task-option)=                              :class:
:PROPERTIES:
:CUSTOM_ID: reference-show
:END:
Whether to hide or show this task in the application.

***** =goal (chronometrist:task-option)=                              :class:
:PROPERTIES:
:CUSTOM_ID: reference-goal
:END:
The daily time goal for this task, in seconds.

***** =property-presets (chronometrist:task-option)=                  :class:
:PROPERTIES:
:CUSTOM_ID: reference-property-presets
:END:
Preset property-value combinations for this task, to be used as suggestions.


** Backend protocol
:PROPERTIES:
:CREATED:  2022-01-05T19:00:12+0530
:CUSTOM_ID: reference-backend
:END:
This section lists the current backend protocol, with some remarks.

#+results: 
**** =backend (standard-object)=                                      :class:
:PROPERTIES:
:CUSTOM_ID: reference-backend
:END:
Protocol class for Chronometrist backends.

**** =file-backend-mixin (standard-object)=                           :class:
:PROPERTIES:
:CUSTOM_ID: reference-file-backend-mixin
:END:
Mixin for backends storing data in a single file.

**** =define-backend (name direct-superclasses direct-slots &rest options)= :macro:function:
:PROPERTIES:
:CUSTOM_ID: reference-define-backend
:END:
Define an instance of =chronometrist:backend=.

Like =defclass=, but also define a =*<name>*= special variable holding an instance of the class.

If DIRECT-SUPERCLASSES do not contain =chronometrist:backend= or a subclass of the same, =chronometrist:backend= is added to the DIRECT-SUPERCLASSES.


*** Writers
:PROPERTIES:
:CUSTOM_ID: reference-backend-writers
:END:
#+results: 
**** =initialize (backend)=                                :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-initialize
:END:
Initialize and return BACKEND.

Disk-based backends should use this to create their file(s), if it does not already exist.

**** =cleanup (backend)=                                   :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-cleanup
:END:
Perform cleanup for BACKEND. Called at application exit.

**** =insert (backend object &key &allow-other-keys)=      :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-insert
:END:
Insert OBJECT into BACKEND.

Return non-nil if insertion is successful.

OBJECT may be an instance of =chronometrist:interval= or =chronometrist:event=.

**** =update (backend old new)=                            :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-update
:END:
In BACKEND, update OLD object with NEW.

OBJECT may be an instance of =chronometrist:interval= or =chronometrist:event=.

**** =remove (backend object &key &allow-other-keys)=      :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-remove
:END:
Remove OBJECT from BACKEND.

Return non-nil if OBJECT is successfully removed.

Signal an error if OBJECT was not found.

OBJECT may be an instance of =chronometrist:interval= or =chronometrist:event=.


*** Extended writer protocol
:PROPERTIES:
:CUSTOM_ID: reference-backend-extended-writers
:END:
#+results: 
**** =clock-in (backend task)=                             :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-clock-in
:END:
Insert a new active interval for TASK to BACKEND.

**** =clock-out (backend interval)=                        :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-clock-out
:END:
In BACKEND, clock out of INTERVAL.

**** =migrate (input-backend output-backend &key if-exists interval-function
&allow-other-keys)= :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-migrate
:END:
Save data from INPUT-BACKEND to OUTPUT-BACKEND.

IF-EXISTS may be :ERROR (the default), :OVERWRITE, or :MERGE.

INTERVAL-FUNCTION should be a function that accepts and returns an instance of =interval=. It is called for every =interval= to be inserted.


*** Readers
:PROPERTIES:
:CUSTOM_ID: reference-backend-readers
:END:
#+results: 
**** =active-backend ()=                                           :function:
:PROPERTIES:
:CUSTOM_ID: reference-active-backend
:END:
Return an object representing the currently active backend.

**** =intervals (backend &key start end)=                  :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-intervals
:END:
Return a list of inactive intervals from BACKEND, or NIL if no intervals were found.

Second return value is a list of active intervals.

Both returned lists should be in reverse chronological order.

If START and END are provided, return intervals intersecting with that time range. START and END should be instances of =local-time:timestamp=.

If END is NIL, return all intervals from START.

If BACKEND is file-based, it must signal a =FILE-DOES-NOT-EXIST= condition if the file does not exist.


*** Extended reader protocol
:PROPERTIES:
:CUSTOM_ID: reference-backend-extended-readers
:END:
#+results: 
**** =latest-interval (backend)=                           :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-latest-interval
:END:
Return the latest interval from BACKEND, or nil if BACKEND contains no intervals.

Return value may be active, i.e. it may or may not have a =stop= value.

If the latest record starts on one day and ends on another, the entire (unsplit) record must be returned.

**** =list-tasks (backend)=                                :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-list-tasks
:END:
Return all tasks recorded in BACKEND as a list of =chronometrist:task= instances.

**** =active-days (backend task &key start end)=           :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-active-days
:END:
From BACKEND, return number of days on which TASK had recorded time.

**** =current-task (&optional (backend (active-backend)))=         :function:
:PROPERTIES:
:CUSTOM_ID: reference-current-task
:END:
Return the name of the active task as a string, or nil if not clocked in.

**** =task-duration (task &key (start (midnight))
(end
 (adjust-timestamp start
   (offset hour 24)))
(backend (active-backend)) (intervals nil intervals-supplied?))= :function:
:PROPERTIES:
:CUSTOM_ID: reference-task-duration
:END:
Return time spent on TASK between START and END.

TASK should be an instance of =chronometrist:task=.

The return value is a duration in seconds, as an integer.


- Change optional arguments to keyword arguments?
- Rename =task-duration-one-day= to =task-duration=, and change the signature to -
  #+begin_src lisp
  (defun task-duration
      (task &key (start (midnight))
              (end (local-time:adjust-timestamp start
                     (offset :hour 24)))
              (backend (active-backend))
              (intervals nil intervals-supplied?))
    ...)
  #+end_src
  Shorter name would result in better indentation and thus easier reading of calls.

*** Conditions
:PROPERTIES:
:CUSTOM_ID: reference-backend-conditions
:END:
#+results: 
**** =no-active-backend=                                          :condition:
:PROPERTIES:
:CUSTOM_ID: reference-no-active-backend
:END:
Condition raised when no active backend has been specified.


*** Of questionable utility
:PROPERTIES:
:CUSTOM_ID: reference-backend-questionable
:END:
1. =on-add (backend)=
2. =on-modify (backend)=
3. =on-remove (backend)=
4. =on-change (backend)=
5. =verify (backend)=
6. =task-records-for-date (backend task date &key &allow-other-keys)= (default method provided)

** Table protocol
:PROPERTIES:
:CREATED:  2022-10-26T20:15:00+0530
:CUSTOM_ID: table-protocol
:END:
#+results: 
**** =table (standard-object)=                                        :class:
:PROPERTIES:
:CUSTOM_ID: reference-table
:END:
A class for tables which are easy to extend with new columns.

**** =schema (object)=                                     :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-schema
:END:
**** =fold-mixin (standard-object)=                                   :class:
:PROPERTIES:
:CUSTOM_ID: reference-fold-mixin
:END:
**** =folded? (object)=                                    :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-folded?
:END:
**** =index-mixin (standard-object)=                                  :class:
:PROPERTIES:
:CUSTOM_ID: reference-index-mixin
:END:
**** =index (object)=                                      :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-index
:END:
**** =row (chronometrist-clim:index-mixin)=                           :class:
:PROPERTIES:
:CUSTOM_ID: reference-row
:END:
**** =foldable-row (chronometrist-clim:fold-mixin chronometrist-clim:row)= :class:
:PROPERTIES:
:CUSTOM_ID: reference-foldable-row
:END:
**** =column (chronometrist-clim:label-mixin)=                        :class:
:PROPERTIES:
:CUSTOM_ID: reference-column
:END:
**** =label (object)=                                      :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-label
:END:
**** =index-column (chronometrist-clim:column chronometrist-clim:index-mixin)= :class:
:PROPERTIES:
:CUSTOM_ID: reference-index-column
:END:
**** =task-name-column (chronometrist-clim:column)=                   :class:
:PROPERTIES:
:CUSTOM_ID: reference-task-name-column
:END:
**** =display-cell (frame pane table column row view)=     :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-display-cell
:END:
Display and return the contents of a cell in TABLE.

FRAME and PANE are the CLIM frame and pane as passed to the display function.

TABLE, ROW, and COLUMN are instances of =table=, =row=, and =column=, respectively.

**** =cell (chronometrist-clim:label-mixin)=                          :class:
:PROPERTIES:
:CUSTOM_ID: reference-cell
:END:
**** =header (chronometrist-clim:cell)=                               :class:
:PROPERTIES:
:CUSTOM_ID: reference-header
:END:
**** =index (chronometrist-clim:cell chronometrist-clim:index-mixin)= :class:
:PROPERTIES:
:CUSTOM_ID: reference-index
:END:
