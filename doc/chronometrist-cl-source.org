#+TITLE: Chronometrist
#+SUBTITLE: Friendly and powerful personal time tracker/analyzer in CLIM
#+EXPORT_FILE_NAME: chronometrist-cl.org
#+DESCRIPTION: User Manual
#+OPTIONS: prop:t toc:3

* Explanation
:PROPERTIES:
:DESCRIPTION: The design, the implementation, and a little history
:CUSTOM_ID: explanation
:END:
Chronometrist is a friendly and powerful personal time tracker and analyzer. This is a Common Lisp port of the [[https://codeberg.org/contrapunctus/chronometrist][Emacs Lisp original]].

Currently, it contains
1. an import-only plist-group backend
2. an SQLite backend (nearly complete)
3. a [[https://github.com/robert-strandh/Clobber][Clobber]] (object store) backend (nearly complete)
4. a [[https://mcclim.common-lisp.dev/][CLIM]] frontend (WIP)

Currently, this port can -
1. import from a plist-group file and export to an SQLite or Clobber database
   #+BEGIN_SRC lisp
   (ql:quickload '(chronometrist-plist chronometrist-clobber))
   (in-package :chronometrist)
   (let ((plg (make-instance 'chronometrist.plist-group:backend
                             :file #P"/path/to/old/file"))
         (clobber (make-instance 'chronometrist.clobber:backend
                                 :file #P"/path/to/new/file")))
     (chronometrist:initialize clobber)
     (chronometrist:migrate plg clobber :if-exists :overwrite))
   #+END_SRC

2. display the CLIM GUI -
   #+BEGIN_SRC lisp
   (ql:quickload '(chronometrist-clim))
   (chronometrist-clim:run)
   #+END_SRC

See [[file:TODO.org][TODO.org]] for ongoing work, future plans, and ideas.

** Characteristics
:PROPERTIES:
:CUSTOM_ID: explanation-characteristics
:END:
1. Made for personal use. By default, your data is stored on your machine and is only accessible to you.
2. Don't make assumptions about the user's profession.
3. (WIP) Hooks to integrate time tracking into your workflow.
4. (WIP) Make it easy to edit and correct data.
5. (WIP) Support mouse and keyboard use equally.
6. (Planned) Extensions to automatically insert data from a variety of sources.

** Motivation for the Common Lisp port
:PROPERTIES:
:CUSTOM_ID: explanation-common-lisp-port
:END:
In March 2022, work began on the long-awaited Common Lisp port of Chronometrist, which aims to create -
1. a greater variety of backends (e.g. SQLite)
2. a common reusable library for frontends to use,
3. a greater variety of frontends, such as -
   * a command line interface (CLI), for UNIX scripting;
   * a terminal user inteface (TUI), for those so inclined;
   * a CLIM (Common Lisp Interface Manager) GUI [fn:1],
   * Qt and Android interfaces using [[https://gitlab.com/eql/lqml][LQML]],
   * web frontends (possibly via [[https://common-lisp.net/project/parenscript/][Parenscript]] or [[https://github.com/rabbibotton/clog][CLOG]]),
   * and perhaps even an interface for wearable devices!

The port was also driven by the desire to have access to Common Lisp's better performance, and features such as namespaces, a /de facto/ standard build system, multithreading, SQLite bindings, a more fully-featured implementation of CLOS and MOP, and type annotations, checking, and inference.

The older Emacs Lisp codebase will probably become an Emacs frontend to a future Common Lisp CLI client.

[fn:1] McCLIM also has an incomplete ncurses backend - when completed, a CLIM frontend could provide a TUI "for free".

** Source code overview
:PROPERTIES:
:CUSTOM_ID: source-code-overview
:END:
*** Data structures
:PROPERTIES:
:CREATED:  2022-07-20T19:56:55+0530
:CUSTOM_ID: explanation-data-structures
:END:
**** Intervals
:PROPERTIES:
:CREATED:  2022-11-05T18:49:21+0530
:CUSTOM_ID: intervals
:END:
Chronometrist stores ranges of time as instances of =interval=. Each =interval= has a =names= (a list of strings), =start= and =stop= timestamps, and optional =properties= (a Lisp property list).

Intervals which represent ongoing activities are said to be "active", and have no =stop= time associated with them.

It is planned to add support for storing events, as instances of =event=. An =event=, much like an =interval=, has a =name= and optional =properties=, but it is only associated with a =time= (timestamp) rather than a time range.

As far as possible, instances of =local-time:timestamp= are used to represent time, on account of the variety of operations available for them. [[file:src/core/time.lisp][time.lisp]] defines various time-related helpers.

**** Tasks
:PROPERTIES:
:CREATED:  2022-11-05T18:49:26+0530
:CUSTOM_ID: tasks
:END:
The concept of a task serves two purposes -
 * Output :: A time tracker may display a flat list of tasks. Some users may prefer a foldable tree instead. The contents and hierarchy of these tasks may differ between users and situations.
 * Input :: A task can be clocked into, i.e. an interval is inserted into the backend with certain tags and/or property-values.

To support this, Chronometrist provides a tree of =task= instances. A "task" is essentially a query, a means of selecting intervals by various means -
1. the names present in the intervals
2. the property-values in the interval property lists
3. whether the intervals pass a given filter function

Each =task= contains a =name= (a string), =depth= (as an integer), an optional =parent= (an instance of =task=), and optional =children= (a list of instances of =task=).

While the task hierarchy affects how data is displayed and entered, neither the hierarchy nor instances of =task= are actually stored in instances of =interval=. This allows the user to change the task hierarchy while minimizing its effect on the data displayed in the frontend.

For instance, if the user has a task tree ~Foo → Bar → Quux~ and inheritance is enabled, clocking into ~Quux~ will create an interval whose =names= are =("Bar" "Foo" "Quux")= (sorted alphabetically). Later, if the user changes their task tree to ~Quux → Bar → Foo~, clocking into ~Foo~ will still create an interval whose =names= are =("Bar" "Foo" "Quux")=. Intervals and durations which appeared under ~Quux~ before will now show up under ~Foo~, because they both have the same ancestors and are equivalent in our view.

***** The basic =task= class
:PROPERTIES:
:CREATED:  2022-11-13T10:28:35+0530
:CUSTOM_ID: basic-task-class
:END:
With the basic =task= class, the user specifies tags, properties, and/or property-values, which are used to determine both what intervals are displayed, as well as what tags, properties, and/or property-values are inserted into new intervals.

***** The =inheriting-task= class
:PROPERTIES:
:CREATED:  2022-11-13T10:29:32+0530
:CUSTOM_ID: inheriting-task-class
:END:
This is designed to create tasks which display the intervals matched by their children, and which may (configurably) insert the tags of their ancestors when creating new intervals.

The user is expected to only specify task names and children. Tags are derived from names.

*** Configuration
:PROPERTIES:
:CREATED:  2022-08-01T20:30:16+0530
:CUSTOM_ID: explanation-configuration
:END:
=chronometrist-cl='s configuration has the following requirements -
1. Users should be able to make persistent changes to the configuration, without requiring Lisp knowledge or changes to the init.lisp
2. Extensions should be able to add new configuration options.

Configuration is implemented using the [[https://github.com/Shinmera/ubiquitous][Ubiquitous]] library.

=chronometrist-cl= has two kinds of configuration options -
1. global, which have a single value applicable to the entire program, and
2. task-specific, which can potentially have a different value for each task.

Ubiquitous stores the configuration as a (serialization of a) hash table with the keys =:global= and =:task=, corresponding to the aforementioned types of configuration.

The =:task= key has another hash table as a value, whose keys are all tasks (as strings), with the exception of a single =:default= key. The value of the =:default= key is a hash table containing default values for task-specific options.

# Add example of configuration

Note that while =define-global-option= and =define-task-option= require a non-keyword symbol as the option name (mostly for consistency with =defun= etc), options are defined as keywords based on the name, and thus all other functions expect options to be keywords.

See also - [[#reference-configuration][reference documentation for the configuration protocol]].

*** CLIM frontend
:PROPERTIES:
:CUSTOM_ID: explanation-clim-frontend
:END:
The CLIM frontend uses CLIM panes for each view of data Chronometrist provides. Each pane has a [[file:chronometrist.org::#display-pane][=display-pane=]] method to display its contents.

Several different CLIM panes implementing different kinds of views have been experimented with, and these are present in the source code, even if not used. Work is underway to make a single "unified" pane, with the aim of presenting a single information-rich view which [[https://web.archive.org/web/20221020163351/http://worrydream.com/MagicInk/][minimizes user interaction]].

**** Tables
:PROPERTIES:
:CUSTOM_ID: explanation-clim-frontend-tables
:END:
The tables in the CLIM frontend are designed to be easy to extend.

The =table= class represents a table. The columns displayed in the table are accessed by the =table-schema (table)= method, which returns a list of =column= instances.

The =row= class provides row-specific information, such as the index of the row. (Provided by default through the =index-mixin= class and =index= accessor.)

The =column= class represents a column descriptor (a list of which forms a =table-schema=), and may be subclassed to provide any column-specific information, such as the label text for the column. (Provided by default through the =label= accessor.) Pre-defined =column= subclasses include =index-column= and =task-column=.

The =display-cell= method -
1. determines the contents of a cell based on the row, column, table, frame, and pane,
2. displays the contents (possibly delegating to a CLIM =present= method), and
3. returns the contents of the cell.
   * This is necessary to be able to mutably hold state as well as aid testing.

** Midnight-spanning intervals
:PROPERTIES:
:DESCRIPTION: Events starting on one day and ending on another
:CUSTOM_ID: explanation-midnight-spanning-intervals
:END:
A unique problem in working with Chronometrist, one I had never foreseen, was intervals which start on one day and end on another. For instance, you start working on something at =2021-01-01 23:00= hours and stop on =2021-01-02 01:00=.

If not accounted for, midnight-spanning intervals can mess up data consumption in all sorts of unforeseen ways. Consider two of the most common operations throughout the program -

1. Attempting to find the intervals belonging to a given date might end up with missing midnight-spanning intervals, near the start and the end of the day;

2. Attempting to find the time spent on a task on a given day can yield inaccurate results. If the day's intervals used for this contain a midnight-spanning interval, the duration will include the previous day's time from the interval as well as the target day's.

There are a few different approaches for dealing with them. In this implementation, all backend reader functions can potentially return midnight-spanning intervals, and it's up to the client code to split them if necessary.

See [[#reference-interval][interval operations]] for functions to split midnight-spanning intervals, check whether two intervals were split from one, and unify two split intervals into one.

** License
:PROPERTIES:
:CUSTOM_ID: explanation-license
:END:
I'd /like/ for all software to be liberated - transparent, trustable, and accessible for anyone to use, study, or improve.

I'd /like/ anyone using my software to credit me for the work.

I'd /like/ to receive financial support for my efforts, so I can spend all my time doing what I find meaningful.

But I don't want to make demands or threats (e.g. via legal conditions) to accomplish all that, nor restrict my services to only those who can pay.

Thus, Chronometrist is released under your choice of [[https://unlicense.org/][Unlicense]] or the [[http://www.wtfpl.net/][WTFPL]].

(See files [[file:UNLICENSE][UNLICENSE]] and [[file:WTFPL][WTFPL]]).

** Thanks
:PROPERTIES:
:CUSTOM_ID: explanation-thanks
:END:
beach for trusting a passionate-but-inexperienced newbie with funding.

jackdaniel and other contributors for laying the foundations for this application through their work on McCLIM, and answering my regular stream of questions.

gilberth, hayley, ck_, rotateq, and many others for helping me out with CLIM and Common Lisp.

The task duration pane was inspired by the Android application, [[https://github.com/netmackan/ATimeTracker][A Time Tracker]]

* Reference
:PROPERTIES:
:CREATED:  2022-07-20T21:28:12+0530
:CUSTOM_ID: reference
:END:
#+BEGIN_SRC lisp :exports none
(ql:quickload '(chronometrist org-cl-ref))
#+END_SRC

# I deliberately create level 4 headings, so I can have a depth of 3
# for the TOC, thereby displaying all reference sections in the ToC,
# without having to show the reference entries.

** Entries
:PROPERTIES:
:CREATED:  2022-07-20T23:51:24+0530
:CUSTOM_ID: reference-entries
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:names
   chronometrist:entry
   chronometrist:interval
   chronometrist:make-interval
   ;; remove `interval' from these functions
   chronometrist:interval-equal?
   chronometrist:interval-duration
   chronometrist:split-interval
   chronometrist:trim-intervals
   chronometrist:intervals-split-p
   chronometrist:interval-unify
   chronometrist:event)
 4 "reference-")
#+END_SRC
** Tasks
:PROPERTIES:
:CREATED:  2022-11-14T10:46:06+0530
:CUSTOM_ID: tasks-1
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist.task:*task-types*
   chronometrist.task:define-task
   chronometrist.task:task
   chronometrist.task:name
   chronometrist.task:tags
   chronometrist.task:properties
   chronometrist.task:property-values
   chronometrist.task:parent
   chronometrist.task:children
   chronometrist.task:depth
   chronometrist.task:tags-match?
   chronometrist.task:properties-match?
   chronometrist.task:property-values-match?
   chronometrist.task:interval-match?
   chronometrist.task:task-interval
   chronometrist.task:task-equal?
   chronometrist.task:ancestors
   chronometrist.task:task-ancestor?
   chronometrist.task:map-task-tree
   chronometrist.task:task-tree-to-list)
 4 "reference-")
#+END_SRC

*** Simple task
:PROPERTIES:
:CREATED:  2022-11-15T18:36:47+0530
:CUSTOM_ID: simple-task
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist.task:simple-task
   chronometrist.task:make-simple-task)
 4 "reference-")
#+END_SRC

*** Inheriting task
:PROPERTIES:
:CREATED:  2022-11-15T18:36:53+0530
:CUSTOM_ID: inheriting-task
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist.task:inheriting-task
   chronometrist.task:make-inheriting-task)
 4 "reference-")
#+END_SRC

** Time operations
:PROPERTIES:
:CREATED:  2022-07-20T23:52:05+0530
:CUSTOM_ID: reference-time
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:apply-time
   chronometrist:midnight)
 4 "reference-")
#+END_SRC
** Configuration protocol
:PROPERTIES:
:CREATED:  2022-07-31T23:13:53+0530
:CUSTOM_ID: reference-configuration
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:option
   chronometrist:global-option
   chronometrist:task-option
   chronometrist:global-value
   (setf chronometrist:global-value)
   chronometrist:task-value
   (setf chronometrist:task-value))
 4 "reference-")
#+END_SRC
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:define-global-option
   chronometrist:define-task-option)
 4 "reference-")
#+END_SRC
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:load-configuration)
 4 "reference-")
#+END_SRC
*** Predefined options
:PROPERTIES:
:CREATED:  2022-08-03T21:09:53+0530
:CUSTOM_ID: predefined-options
:END:
**** Global options
:PROPERTIES:
:CREATED:  2022-08-03T21:11:33+0530
:CUSTOM_ID: global-options
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:database-directory
   chronometrist:database-pathname-name
   chronometrist:init-pathname
   chronometrist:allow-concurrent-intervals
   chronometrist:day-start-time
   chronometrist:week-start-day)
 5 "reference-")
#+END_SRC

**** Task options
:PROPERTIES:
:CREATED:  2022-08-03T21:11:35+0530
:CUSTOM_ID: task-options
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:show
   chronometrist:goal
   chronometrist:property-presets)
 5 "reference-")
#+END_SRC

** Backend protocol
:PROPERTIES:
:CREATED:  2022-01-05T19:00:12+0530
:CUSTOM_ID: reference-backend
:END:
This section lists the current backend protocol, with some remarks.

#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:backend
   chronometrist:file-backend-mixin
   chronometrist:define-backend)
 4 "reference-")
#+END_SRC

*** Writers
:PROPERTIES:
:CUSTOM_ID: reference-backend-writers
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:initialize
   chronometrist:cleanup
   chronometrist:insert
   chronometrist:update
   chronometrist:remove)
 4 "reference-")
#+END_SRC

*** Extended writer protocol
:PROPERTIES:
:CUSTOM_ID: reference-backend-extended-writers
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:clock-in
   chronometrist:clock-out
   chronometrist:migrate)
 4 "reference-")
#+END_SRC
*** Readers
:PROPERTIES:
:CUSTOM_ID: reference-backend-readers
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:active-backend
   chronometrist:intervals)
 4 "reference-")
#+END_SRC
*** Extended reader protocol
:PROPERTIES:
:CUSTOM_ID: reference-backend-extended-readers
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:latest-interval
   chronometrist:list-tasks
   chronometrist:active-days
   chronometrist:current-task
   chronometrist:task-duration)
 4 "reference-")
#+END_SRC
   * Change optional arguments to keyword arguments?
   * Rename =task-duration-one-day= to =task-duration=, and change the signature to -
     #+BEGIN_SRC lisp
(defun task-duration
    (task &key (start (midnight))
            (end (local-time:adjust-timestamp start
                   (offset :hour 24)))
            (backend (active-backend))
            (intervals nil intervals-supplied?))
  ...)
     #+END_SRC
     Shorter name would result in better indentation and thus easier reading of calls.

*** Conditions
:PROPERTIES:
:CUSTOM_ID: reference-backend-conditions
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist:no-active-backend)
 4 "reference-")
#+END_SRC
*** Of questionable utility
:PROPERTIES:
:CUSTOM_ID: reference-backend-questionable
:END:
1. =on-add (backend)=
2. =on-modify (backend)=
3. =on-remove (backend)=
4. =on-change (backend)=
5. =verify (backend)=
6. =task-records-for-date (backend task date &key &allow-other-keys)= (default method provided)

** Table protocol
:PROPERTIES:
:CREATED:  2022-10-26T20:15:00+0530
:CUSTOM_ID: table-protocol
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(chronometrist-clim:table
   chronometrist-clim:schema
   chronometrist-clim:fold-mixin
   chronometrist-clim:folded?
   chronometrist-clim:index-mixin
   (chronometrist-clim:index :type definitions:function)
   chronometrist-clim:row
   chronometrist-clim:foldable-row
   chronometrist-clim:column
   chronometrist-clim:label
   chronometrist-clim:index-column
   chronometrist-clim:task-name-column
   chronometrist-clim:display-cell
   chronometrist-clim:cell
   chronometrist-clim:header
   (chronometrist-clim:index :type definitions:class))
 4 "reference-")
#+END_SRC

* Local variables                                                  :noexport:
:PROPERTIES:
:CREATED:  2023-03-02T20:06:51+0530
:CUSTOM_ID: local-variables
:END:
# Local Variables:
# org-confirm-babel-evaluate: nil
# eval: (require 'ox-org)
# End:
