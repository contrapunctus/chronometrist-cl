(defsystem     "chronometrist-sqlite"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Friendly and extensible personal time tracker - SQLite backend"
  :depends-on  (:chronometrist :sqlite :sxql)
  :serial      t
  :components  ((:module "src/backend/"
                 :serial t
                 :components ((:file "sqlite")))))
