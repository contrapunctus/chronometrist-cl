(defsystem     "chronometrist-tests"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Friendly and extensible personal time tracker - tests"
  :depends-on  (:chronometrist-clobber :fiveam)
  :serial      t
  :components  ((:module "tests/"
                 :serial t
                 :components ((:file "package")
                              (:file "backend")))))
