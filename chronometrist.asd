(defsystem     "chronometrist"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Friendly and extensible personal time tracker - common library"
  :depends-on  (:trivia
                :alexandria
                :serapeum
                :rutils
                :metabang-bind
                :fn
                :named-readtables
                :anaphora
                :local-time
                :format-seconds
                :ubiquitous
                :trivial-types)
  :serial      t
  :components  ((:module "src/core/"
                 :serial t
                 :components ((:file "package")
                              (:file "helpers")
                              (:file "time")
                              (:file "configuration")
                              (:file "interval")
                              (:module "task/"
                               :serial t
                               :components ((:file "package")
                                            (:file "core")
                                            (:file "simple")
                                            (:file "inheriting")
                                            (:file "property")))
                              (:file "protocol-backend")
                              (:file "protocol-backend-extended")
                              (:file "migration")))))
